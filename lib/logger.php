<?php

/**
 * 
 * @author Tibor Csik <csulok0000@gmail.com>
 * @copyright 2015 (c), Tibor Csik
 */

namespace SEngine;

class Logger extends AbstractLogger {
    
    /**
     *
     * @var string
     */
    protected $logFile = 'log.log';
    
    protected $handle;
    
    /**
     * 
     * @param string $logDir
     */
    public function __construct($logDir, $name = '') {
        
        if (!is_dir($logDir) || !is_writeable($logDir)) {
            self::$enabled = false;
        }
        
        $path = $logDir . '/' . ($name ? $name : 'default') . '/';
        if (self::$enabled) {
            if (!is_dir($path)) {
                mkdir($path, 0777);
            }
        }
        
        $this->logFile = $path . date('Y-m-d') . '.log';
        
        parent::__construct();
    }
    
    public function __destruct() {
        if (is_resource($this->handle)) {
            fclose($this->handle);
        }
    }
    
    /**
     * 
     * @param string $message
     * @param string $type
     */
    public function log($message, $type = self::TYPE_INFO) {
        if (!self::$enabled) {
            return $this;
        }
        
        if (!is_resource($this->handle)) {
            $this->handle = fopen($this->logFile, 'a+');
        }
        
        $limit = 100000;
        if (strlen($message) > $limit) { // 100 000 karakter felett levágja
            $message = substr($message, 0, $limit) . '...';
        }
        
        $type = str_pad(substr($type, 0, 8), 8, ' ', STR_PAD_RIGHT);
        fwrite($this->handle, date('H:i:s') . "\t" . $this->getSession() . "\t" . strtoupper($type) . "\t" . str_replace(array("\r\n", "\r", "\n"), "\\n", $message) . "\n");
        return $this;
    }
}