<?php

/**
 * 
 * @author Tibor Csik <csulok0000@gmail.com>
 * @copyright 2015 (c), Tibor Csik
 */

namespace SEngine;

class JsonResponse extends Response {
    
    /**
     * 
     * @param array $data
     */
    public function __construct($data) {
        parent::__construct($data, Response::TYPE_JSON);
    }
}