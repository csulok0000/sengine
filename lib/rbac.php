<?php

/**
 * 
 * @author Tibor Csik <csulok0000@gmail.com>
 * @copyright 2015 (c), Tibor Csik
 */

namespace SEngine;

class Rbac implements RbacInterface {
    
    /**
     *
     * @var array
     */
    protected $roles = array();
    
    /**
     *
     * @var array
     */
    protected $permissions = array();
    
    /**
     * 
     * Initialize RBAC
     * 
     * Roles:
     *  [
     *      <role name 1> => [
     *          <permission name 1>,
     *          <permission name 2>,
     *          <permission name 3>,
     *          <permission name 4>
     *      ],
     *      <role name 2> => [
     *          <permission name 1>,
     *          <permission name 2>,
     *          <permission name 3>,
     *          <permission name 4>
     *      ],
     *  ]
     * 
     * @param array $roles Roles of the entity
     */
    public function __construct(array $roles) {
        $this->roles = $roles;
        $this->rebuildPermissions();
    }
    
    /**
     * 
     * @param string $permissions
     * @return bool
     */
    public function hasPermission($permissions) {
        $tmp = explode(',', $permissions);
        foreach ($tmp as $permission) {
            if (in_array(trim($permission), $this->permissions)) {
                return true;
            }
        }
        return false;
    }
    
    /**
     * 
     * @param string $key
     * @param bool $isRole
     * @return bool
     */
    public function isAuthorized($key, $isRole = false) {
        if ($isRole) {
            return $this->hasRole($key);
        }
        
        return $this->hasPermission($key);
    }
    
    /**
     * 
     * @param string $roles
     * @return bool
     */
    public function hasRole($roles) {
        $roleList = explode(',', $roles);
        foreach ($roleList as $role) {
            if (array_key_exists(trim($role), $this->roles)) {
                return true;
            }
        }
        return false;
    }
    
    /**
     * 
     * Clear all roles and permissions
     *  
     * @return $this
     */
    public function clear() {
        $this->permissions = array();
        $this->roles = array();
        return $this;
    }
    
    /**
     * 
     * @return array
     */
    public function getRoles() {
        return array_keys($this->roles);
    }
    
    public function getPermissions() {
        return array_keys($this->permissions);
    }
    
    /**
     * 
     * @param array $roles
     * @return $this
     */
    public function addRoles(array $roles) {
        foreach ($roles as $role => $permissions) {
            $this->addRole($role, $permissions);
        }
        
        return $this;
    }
    
    /**
     * 
     * @param string $roleName
     * @param array $permissions
     * @return $this
     */
    public function addRole($roleName, array $permissions) {
        $this->roles[$roleName] = $permissions;
        $this->rebuildPermissions();
        return $this;
    }
    
    /**
     * 
     * @param string $roleName
     * @return $this
     */
    public function removeRole($roleName) {
        if (isset($this->roles[$roleName])) {
            unset($this->roles[$roleName]);
            $this->rebuildPermissions();
        }
        return $this;
    }
    
    /**
     * 
     * @return $this
     */
    public function rebuildPermissions() {
        foreach ($this->roles as $role) {
            foreach ($role as $permission) {
                $this->permissions[$permission] = $permission;
            }
        }
        return $this;
    }
}