<?php

/**
 * 
 * @author Tibor Csik <csulok0000@gmail.com>
 * @copyright 2015 (c), Tibor Csik
 */

namespace SEngine;

use Exception;

class Response implements ResponseInterface {

    const HEADER_301 = 'HTTP/1.1 301 Moved Permanently';
    const HEADER_302 = 'HTTP/1.1 302 Found';
    
    /**
     * Constants for Response Type
     */
    const TYPE_HTML = 1;
    const TYPE_AJAX = 2;
    const TYPE_JSON = 3;
    
    /**
     *
     * @var string
     */
    protected $html;
    
    /**
     *
     * @var array
     */
    protected $json;
    
    /**
     *
     * @var string
     */
    protected $type = Response::TYPE_HTML;
    
    /**
     *
     * @var string
     */
    protected $redirect = '';
    
    /**
     *
     * @var string[]
     */
    protected $headers = [];
    
    /**
     * 
     * @param mixed $data
     * @param int $type Values: Response::TYPE_*
     * @throws Exception
     */
    public function __construct($data = '', $type = Response::TYPE_HTML) {
        if (!in_array($type, array(Response::TYPE_AJAX, Response::TYPE_HTML, Response::TYPE_JSON))) {
            throw new Exception('Invalid repsonse type!');
        }
        $this->type = $type;
        
        if ($this->type == Response::TYPE_HTML) {
            if (!is_string($data)) {
                throw new Exception('Data paramater must be string when using Response::TYPE_HTML response type!');
            }
            $this->html = $data;
        } elseif ($this->type == Response::TYPE_AJAX || $this->type == Response::TYPE_JSON) {
            $this->json = $data;
        }
    }
    
    /**
     * 
     * @return string
     * @throws Exception
     */
    public function __toString() {
        if ($this->type == Response::TYPE_HTML) {
            return $this->html;
        } elseif ($this->type == Response::TYPE_AJAX) {
            if (!array_key_exists('message', $this->json)) {
                $this->json['message'] = '';
            }
            if (!array_key_exists('success', $this->json)) {
                $this->json['success'] = 'OK';
            }
            return json_encode($this->json);
        } elseif ($this->type == Response::TYPE_JSON) {
            return json_encode($this->json);
        } else {
            throw new Exception('Invalid response type!');
        }
    }
    
    /**
     * 
     * @param string $header
     * @return $this
     */
    public function addHeader($header) {
        $this->headers[] = $header;
        return $this;
    }
    
    /**
     * 
     * @param array $headers
     * @return $this
     */
    public function setHeaders(array $headers) {
        $this->headers = $headers;
        return $this;
    }
        
    
    /**
     * 
     * Set the html value for response, and set response type to Response::TYPE_HTML
     * 
     * @param string $html
     */
    public function setHTML($html) {
        $this->type = Response::TYPE_HTML;
        $this->html = $html;
    }
    
    /**
     * 
     * Set the json value for response, and set response type to Response::TYPE_AJAX
     * 
     * @param array $json
     */
    public function setJSON($json) {
        $this->type = Response::TYPE_AJAX;
        $this->json = $json;
    }
    
    /**
     * 
     * @return int
     */
    public function getType() {
        return $this->type;
    }
    
    /**
     * 
     * @param string $url
     */
    public function redirect($url) {
        $this->redirect = $url;
    }
    
    /**
     * 
     * @return string
     */
    public function getRedirectUrl() {
        return $this->redirect;
    }
    
    public function jsonSerialize(): string {
        return (string) $this;
    }
    
    public function getJSON() {
        return $this->json;
    }
    
    /**
     * 
     * A beállított fejléc adatokat adja vissza tömbként.
     * 
     * @return string[]
     */
    public function getHeaders() {
        return $this->headers;
    }
}