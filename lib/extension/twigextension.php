<?php

/**
 * 
 * @author Tibor Csik <csulok0000@gmail.com>
 * @copyright 2015 (c), Tibor Csik
 */

namespace SEngine;

use Twig_Function_Method;
use Twig_SimpleFilter;

class TwigExtension extends \Twig_Extension {

    /**
     * 
     * @return array
     */
    public function getFunctions() {
        return array(
            'microtime' => new Twig_Function_Method($this, '_microtime'),
            'routing' => new Twig_Function_Method($this, '_routing', array('needs_context' => true)),
            'link' => new Twig_Function_Method($this, '_routing', array('needs_context' => true)), // alias for routing
            'isCurrentRoute' => new Twig_Function_Method($this, '_isCurrentRoute', array('needs_context' => true)),
            'hasPermission' => new Twig_Function_Method($this, '_hasPermission', array('needs_context' => true)),
            'hasRole' => new Twig_Function_Method($this, '_hasRole', array('needs_context' => true)),
            'render' => new Twig_Function_Method($this, '_render', array('needs_context' => true, 'is_safe' => array('html')))
        );
    }
    
    public function getFilters() {
        return array(
            new Twig_SimpleFilter('strPad', array($this, '_strpad')),
        );
    }
    
    /**
     * 
     * @return string
     */
    public function _microtime() {
        return microtime(true);
    }
    
    /**
     * 
     * @param array $context
     * @param string $route
     * @param array $params
     * @param bool $fullUrl
     * @return string
     */
   public function _routing($context, $route, $params = array(), $fullUrl = false) {
        /*@var $app Engine */
        $app = $context['app'];
        if ($route) {
            $config = $app->getConfig();
                return ($fullUrl ? $config->get('site->host') : '') .  $app->getRouter()->getUrl($route, $params);
        } else {
                trigger_error('Invalid route!', E_USER_NOTICE);
        }
   }    
   
   /**
    * 
    * @param array $context
    * @param string $routeNames
    * @return string
    */
   public function _isCurrentRoute($context, $routeNames) {
        /*@var $app Engine */
       
        $app = $context['app'];
        $route = $app->getRouter()->getRoute();
        
        if ($route) {
            $routeNames = explode(',', str_replace(' ', ',', $routeNames));
            foreach ($routeNames as $routeName) {
                $routeName = trim($routeName, "\r\n\t ");
                if (substr($routeName, -1) == '*') {
                    $routeName = trim($routeName, '*');
                    if (strpos($route->getRouteName(), $routeName) === 0) {
                        return true;
                    }
                } elseif ($route->getRouteName() == $routeName) {
                    return true;
                }
            }
        }
        return false;
   }
   
   /**
    * 
    * @param array $context
    * @param string $permissions
    * @return string
    */
   public function _hasPermission($context, $permissions) {
        /*@var $app Engine */
        $app = $context['app'];
        
        return $app->getRbac()->hasPermission($permissions);
   }    
   /**
    * 
    * @param array $context
    * @param string $roles
    * @return string
    */
   public function _hasRole($context, $roles) {
        /*@var $app Engine */
        $app = $context['app'];
        
        return $app->getRbac()->hasRole($roles);
   }    
   
   /**
    *
    * @param array $context
    * @param string $target
    * @param array $params
    * @return string
    */
    public function _render($context, $target, $params = array()) {
        $app = $context['app'];
        if ($target) {
            list($scope, $moduleClass, $method) = explode(':', $target);
            //require_once(MODULE_DIR . '/' . strtolower($scope) . '/' . strtolower($moduleClass) . '.php');
            $moduleClass = ($scope ? $scope . '_' : '') . $moduleClass;
            if (!class_exists($moduleClass)) {
                if ($scope) {
                    $moduleClass = substr($moduleClass, strlen($scope) + 1);
                }
                
                if (!class_exists($moduleClass)) {
                    throw new NotFoundException('Module not exists: ' . $moduleClass . '!');
                }
            }

            $module = new $moduleClass($app);
            
            if (!method_exists($module, $method)) {
                throw new NotFoundException('Action not exists: ' . $method . '!');
            }

            $response = call_user_func_array(array($module, $method), $params);

            if (!$response) {
                throw new NotFoundException('Empty response! ' . $moduleClass . ':' . $method);
            }

            return $response;
        } else {
            trigger_error('Target is undefined', E_USER_NOTICE);
        }
    }
    
    function _strpad($input, $padlength, $padstring = '', $padtype = 'right') {
        $type = \STR_PAD_RIGHT;
        
        switch ($padtype) {
            case 'left': $type = \STR_PAD_LEFT; break;
        }
        
        return \str_pad($input, $padlength, $padstring, $type);
    }

    /**
     * 
     * @return String
     */
    public function getName() {
        return "sengine_extension";
    }

}