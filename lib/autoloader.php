<?php

/**
 * 
 * @author Tibor Csik <csulok0000@gmail.com>
 * @copyright 2015 (c), Tibor Csik
 */

namespace SEngine;

class AutoLoader {
    
    /**
     *
     * __construct
     */
    function __construct() {
        spl_autoload_register(array($this, 'autoLoad'));
    }

    /**
     *
     * autoLoad
     *
     * @param string $className
     */
    function autoLoad($className) {
        if (!strstr($className, 'SEngine')) {
            return;
        }
        
        $lib = '';
        
        // set class
        $tmp = explode('\\', $className);
        $class = strtolower(end($tmp));
        
        
        /*
        // set namespace
        unset($tmp[count($tmp) - 1]);
        $namespace = '\\' . implode('\\', $tmp);
        unset($tmp);
        */
        
        // Régi elnevezés alapján
        if (substr($class, 0, 8) == 'abstract') {
            // Abstract classes
            $lib = __DIR__ . '/abstract/' . $class . '.php';
        } else {
            if (substr($class, -8) == 'abstract') {
                // Abstract
                $lib = __DIR__ . '/abstract/' . substr($class, 0, -8) . '.php';
            } elseif (substr($class, -9) == 'interface') {
                // Interface
                $lib = __DIR__ . '/interface/' . substr($class, 0, -9) . '.php';
            } elseif (substr($class, -9) == 'exception') {
                // Exception
                $lib = __DIR__ . '/exception/' . $class . '.php';
            } elseif (substr($class, -9) == 'extension') {
                // Exception
                $lib = __DIR__ . '/extension/' . $class . '.php';
            } elseif (substr($class, -7) == 'factory') {
                // Factory
                $lib = __DIR__ . '/factory/' . substr($class, 0, -7) . '.php';
            } else {
                // Lib
                $lib = __DIR__ . '/' . $class . '.php';
            }
        }

        // Load class
        if ($lib && file_exists($lib)) {
            require_once($lib);
        }
    }
}