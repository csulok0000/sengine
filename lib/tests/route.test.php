<?php

/**
 * 
 * @author Tibor Csik <csulok0000@gmail.com>
 * @copyright 2015 (c), Tibor Csik
 */

require_once(__DIR__ . '/../route.php');

class RouteTest extends PHPUnit_Framework_TestCase {
    
    public function testBasic() {
        
        $route = new Route('Test Route Name', 'Test Module', 'Test Method', 'Test Template', array('param1' => 'Value1'), 'Test Scope');
        
        $this->assertEquals('Test Route Name', $route->getRouteName());
        $this->assertEquals('Test Module', $route->getModule());
        $this->assertEquals('Test Method', $route->getMethod());
        $this->assertEquals('Test Template', $route->getTemplate());
        $this->assertEquals('Test Scope', $route->getScope());
        
        $this->assertArrayHasKey('param1', $route->getParameters());
        $this->assertContains('Value1', $route->getParameters());
        
    }
}