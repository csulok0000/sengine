<?php

/**
 * 
 * @author Tibor Csik <csulok0000@gmail.com>
 * @copyright 2015 (c), Tibor Csik
 */

require_once(__DIR__ . '/../config.php');

class ConfigTest extends PHPUnit_Framework_TestCase {
    
    public function testConfig() {
        $rawConfig = array(
            'section' => array(
                'attr1' => 'value1',
                'attr2' => 'value2',
                'attr3' => 'value3',
                'attr4' => 'value4'
            ),
            'section2' => array(
                'attr1' => 'value8',
                'attr5' => 'value5',
                'attr6' => 'value6',
                'attr7' => 'value7'
            ),
            'section.test' => array(
                'attr1' => 'value9',
                'attr10' => 'value10'
            ),
            'section.test2' => array(
                'attr1' => 'value11',
                'attr12' => 'value12'
            )
        );
        
        $this->assertNotEmpty($rawConfig);
        
        return $rawConfig;
    }
    
    public function testEmpty() {
        $config = new Config(array());
        
        $this->assertEmpty($config->getConfigArray());
        $this->assertNull($config->get('test'));
        
    }
    
    /**
     * 
     * @depends testConfig
     */
    public function testWithoutSuffix(array $rawConfig) {
        
        $config = new Config($rawConfig);
        
        $this->assertNotEmpty($config->getConfigArray());
        $this->assertEquals('value1', $config->get('section->attr1'));
        $this->assertEquals('value8', $config->get('section2->attr1'));
        $this->assertEquals('value4', $config->get('section->attr4'));
        $this->assertEquals('value7', $config->get('section2->attr7'));
        $this->assertNull($config->get('section.test->attr1'));
        $this->assertNull($config->get('section.test2->attr4'));
        $this->assertNull($config->get('section3->notExists'));
    }
    
    /**
     * 
     * @depends testConfig
     */
    public function testWithSuffix(array $rawConfig) {
        
        $config = new Config($rawConfig, 'test');
        
        $this->assertNotEmpty($config->getConfigArray());
        $this->assertEquals('value9', $config->get('section->attr1'));
        $this->assertEquals('value10', $config->get('section->attr10'));
        $this->assertEquals('value8', $config->get('section2->attr1'));
        $this->assertEquals('value4', $config->get('section->attr4'));
        $this->assertEquals('value7', $config->get('section2->attr7'));
        $this->assertNull($config->get('section.test->attr1'));
        $this->assertNull($config->get('section.test2->attr4'));
        $this->assertNull($config->get('section3->notExists'));
    }
    
    /**
     * 
     * @depends testConfig
     */
    public function testWithSuffix2(array $rawConfig) {
        
        $config = new Config($rawConfig, 'test2');
        
        $this->assertNotEmpty($config->getConfigArray());
        $this->assertEquals('value11', $config->get('section->attr1'));
        $this->assertEquals('value12', $config->get('section->attr12'));
        $this->assertEquals('value8', $config->get('section2->attr1'));
        $this->assertEquals('value4', $config->get('section->attr4'));
        $this->assertEquals('value7', $config->get('section2->attr7'));
        $this->assertNull($config->get('section->attr10'));
        $this->assertNull($config->get('section.test2->attr4'));
        $this->assertNull($config->get('section3->notExists'));
    }
}