<?php

/**
 * 
 * @author Tibor Csik <csulok0000@gmail.com>
 * @copyright 2015 (c), Tibor Csik
 */

require_once(__DIR__ . '/../breadcrumbs.php');

class BreadCrumbsTest extends PHPUnit_Framework_TestCase {
    
    public function testBasic() {
        $breadCrumbs = new BreadCrumbs();
        
        $this->assertEmpty($breadCrumbs->toArray());
        
        $breadCrumbs->add('Test Name');
        
        $this->assertNotEmpty($breadCrumbs->toArray());
        $this->assertContains('Test Name', current($breadCrumbs->toArray()));
        
        $breadCrumbs->add('Test Name 2', 'Test Url');
        
        $this->assertContains('Test Name', reset($breadCrumbs->toArray()));
        $this->assertContains('Test Name 2', end($breadCrumbs->toArray()));
    }
}