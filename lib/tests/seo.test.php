<?php

/**
 * 
 * @author Tibor Csik <csulok0000@gmail.com>
 * @copyright 2015 (c), Tibor Csik
 */

require_once(__DIR__ . '/../interface/seointerface.php');
require_once(__DIR__ . '/../interface/opengraphinterface.php');
require_once(__DIR__ . '/../interface/opengraphseointerface.php');
require_once(__DIR__ . '/../abstract/abstractseo.php');
require_once(__DIR__ . '/../seo.php');

class SeoTest extends PHPUnit_Framework_TestCase {
    
    public function testEmpty() {
        
        $seo = new Seo();
        
        $this->assertNull($seo->getCanonicalUrl());
        $this->assertNull($seo->getMetaDescription());
        $this->assertNull($seo->getMetaKeywords());
        $this->assertNull($seo->getOgDescription());
        $this->assertNull($seo->getOgImage());
        $this->assertNull($seo->getOgTitle());
        $this->assertNull($seo->getOgType());
        $this->assertNull($seo->getOgUrl());
        $this->assertNull($seo->getTitle());
        
    }
    
    public function testFull() {
        
        $seo = new Seo();
        
        $seo->setCanonicalUrl('Test Canonical');
        $seo->setMetaDescription('Test MetaDesc');
        $seo->setMetaKeywords('Test MetaKeyw');
        $seo->setOgDescription('Test OgDesc');
        $seo->setOgImage('Test OgImage');
        $seo->setOgTitle('Test OgTitle');
        $seo->setOgType('Test OgType');
        $seo->setOgUrl('Test OgUrl');
        $seo->setTitle('Test Title');
        
        $this->assertEquals('Test Canonical', $seo->getCanonicalUrl());
        $this->assertEquals('Test MetaDesc', $seo->getMetaDescription());
        $this->assertEquals('Test MetaKeyw', $seo->getMetaKeywords());
        $this->assertEquals('Test OgDesc', $seo->getOgDescription());
        $this->assertEquals('Test OgImage', $seo->getOgImage());
        $this->assertEquals('Test OgTitle', $seo->getOgTitle());
        $this->assertEquals('Test OgType', $seo->getOgType());
        $this->assertEquals('Test OgUrl', $seo->getOgUrl());
        $this->assertEquals('Test Title', $seo->getTitle());
        
    }
}