<?php

/**
 * 
 * @author Tibor Csik <csulok0000@gmail.com>
 * @copyright (c) 2015, Tibor Csik
 */

namespace SEngine;

use Exception;

class RouteLoader {
    
    /**
     *
     * @var string
     */
    protected $host         = '';
    
    /**
     *
     * @var string
     */
    protected $requestUri   = '';
    
    /**
     *
     * @var string
     */
    protected $routeFileName    = '';
    
    /**
     *
     * @var array
     */
    public $routeTable      = [];
    
    /**
     * 
     * @param string $routeFileName
     * @param string $host
     * @param string $requestUri
     * @return boolean
     * @throws \SEngine\Exception
     */
    public function __construct($routeFileName, $host, $requestUri) {
        $this->host             = $host;
        $this->requestUri       = $requestUri;
        $this->routeFileName    = $routeFileName;
        
        //
        // Route file feldolgozása
        //
        $this->parse($this->routeFileName);
    }
    
    /**
     * 
     * @param string $routeFileName
     * @return boolean
     * @throws \Exception
     */
    protected function parse($routeFileName) {
        $tmp = parse_ini_file($routeFileName, true);
        
        if (!isset($tmp['routeFile']) || !isset($tmp['routeFile']['version']) || $tmp['routeFile']['version'] < '2.0') {
            $this->routeTable = $tmp;
            return true;
        }
        
        unset($tmp['routeFile']);
        
        /**
         * Az aktuális munka könyvtárat eltároljuk és át váltunk a route file könyvtárára
         * Erre azért van szükség hogy leegyszerűsítse az abszolút és relatív fájl elérések kezelését
         */
        $cwd = getcwd();
        chdir(dirname($routeFileName));
        
        try {
            do {
                reset($tmp);
                
                $data = current($tmp);
                $key = key($tmp);
                
                unset($tmp[$key]);
                
                if (substr($key, 0, 1) != '@') {
                    
                    $this->routeTable[$key] = $data;
                    continue;
                }
                
                $host = isset($data['host']) ? $data['host'] : '*';
                $prefix = isset($data['prefix']) ? $data['prefix'] : '*';
                $route = $data['route'];
               
                //$data['host'] = '';
                //$this->routeTable[$key] = $data;

                if ($host && $host !== '*' && $this->host != $host) {
                    continue;
                }

                /**
                 * Prefix ellenőrzés
                 */
                if ($prefix && $prefix !== '*' && strpos($this->requestUri, $prefix) !== 0) {
                    continue;
                }

                /**
                 * Ha ideáig eljutottunk akkor betöltjük a route-ban megadott route fájlt
                 */
                if (!file_exists($route)) {
                    throw new RouteNotFoundException('Route file not found: ' . $route);
                }
                
                $tmp2 = parse_ini_file($route, true);
                
                $tmp = (array)$tmp2 + (array)$tmp;
                
            } while (current($tmp));
            
        } catch (Exception $e) {
            /**
             * Visszaállunk az eredeti mappára, mindenképp
             */
            chdir($cwd);
            throw $e;
        }
        
        /**
         * Visszaállunk az eredeti mappára
         */
        chdir($cwd);
        
        return true;
    }
}