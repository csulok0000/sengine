<?php

/**
 * 
 * @author Tibor Csik <csulok0000@gmail.com>
 * @copyright 2015 (c), Tibor Csik
 */

namespace SEngine;

class BreadCrumbs implements BreadCrumbsInterface {
    
    /**
     *
     * @var array
     */
    protected $data = array();
    
    /**
     * 
     * @param string $name
     * @param string $link
     * @return int
     */
    public function add($name, $link = 'null') {
        $this->data[] = array(
            'name' => $name,
            'link' => $link
        );
        return $this;
    }
    
    /**
     * 
     * @return array
     */
    public function toArray() {
        return $this->data;
    }
}