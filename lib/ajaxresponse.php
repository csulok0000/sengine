<?php

/**
 * 
 * @author Tibor Csik <csulok0000@gmail.com>
 * @copyright 2015 (c), Tibor Csik
 */

namespace SEngine;

class AjaxResponse extends Response {
    
    /**
     * 
     * @param array $data
     */
    public function __construct($success = true, $message = '', $data = array()) {
        parent::__construct(array(
            'success' => $success,
            'message' => $message,
            'data' => $data
        ), Response::TYPE_AJAX);
    }
}