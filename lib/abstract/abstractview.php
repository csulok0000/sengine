<?php

/**
 * 
 * @author Tibor Csik <csulok0000@gmail.com>
 * @copyright 2015 (c), Tibor Csik
 */

namespace SEngine;
 
/**
 * 
 * @deprecated since version v2.0
 */
abstract class AbstractView implements ViewInterface {

	/**
	 * @var array
	 */
	protected $variables = array();
	
	/**
	 *
	 * @abstract
	 * @param string $templateDir
	 * @param string $cacheDir
	 * @param bool $forceReload Reload template file at everytime
	 */
	abstract public function __construct($templateDir, $cacheDir = '', $forceReload = false);
	
	/**
	 *
	 * @abstract
	 * @param string $template
	 * @param array $params Template variables
	 * @return Response
	 */
	abstract public function render($template, $params = array());
	
	/**
	 *
	 * @param string $name
	 * @param mixed $value
	 */
	public function addVar($name, $value) {
		$this->variables[$name] = $value;
	}
	
	/**
	 *
	 * @param string $name
	 */
	public function removeVar($name) {
		if (array_key_exists($name, $this->variables)) {
			unset($this->variables[$name]);
		}
	}
}