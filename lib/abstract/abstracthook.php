<?php

/**
 * 
 * @author Tibor Csik <csulok0000@gmail.com>
 * @copyright 2015 (c), Tibor Csik
 */

namespace SEngine;

/**
 * 
 * @deprecated since version v2.0
 */
abstract class AbstractHook {
    
    /**
     *
     * @var EngineInterface
     */
    protected $engine = null;
    
    /**
     * 
     * @param EngineInterface $engine
     */
    public function __construct(EngineInterface $engine) {
        $this->engine = $engine;
    }
    
    /**
     * 
     * @return EngineInterface
     */
    public function getEngine() {
        return $this->engine;
    }
}