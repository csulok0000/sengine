<?php

/**
 * 
 * @author Tibor Csik <csulok0000@gmail.com>
 * @copyright (c) 2016, Tibor Csik
 */

namespace SEngine\Lib;

abstract class CronJobAbstract extends ModuleAbstract {
    
    abstract function run();
}