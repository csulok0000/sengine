<?php

/**
 * 
 * @author Tibor Csik <csulok0000@gmail.com>
 * @copyright 2015 (c), Tibor Csik
 */

namespace SEngine;

use Pdo;

/**
 * 
 * @deprecated since version v2.0
 */
abstract class AbstractDao extends Lib\DaoAbstract {}