<?php

/**
 * 
 * @author Tibor Csik <csulok0000@gmail.com>
 */
namespace SEngine\Lib;

abstract class CronAbstract {
    
    /**
     *
     * @var Engine
     */
    protected $engine = null;
    
    /**
     *
     * @var array
     */
    protected $jobs = array();
    
    /**
     * 
     * @param \SEngine\EngineInterface $engine
     */
    public function __construct(\SEngine\EngineInterface $engine) {
        $this->engine = $engine;
    }
    
    /**
     * 
     * @param string $time
     * @param string $cron
     * @throws Exception
     */
    public function register($time, $cronName) {
        $time = trim($time) . ' ';
        if (!preg_match('/^([\*\,\/0-9]*[\ \t]){5}$/', $time)) {
            throw new Exception('Invalid cron time: ' . $time);
        }
        
        $this->jobs[] = array(
            'time' => trim($time),
            'cron' => substr($cronName, -7) == 'CronJob' ? $cronName : $cronName . 'CronJob'
        );
    }
    
    /**
     * 
     * Run cronjobs
     */
    public function work() {
        // minute | hour | day | month | dayOfWeek
        $minute     = date('i');
        $hour       = date('G');
        $day        = date('j');
        $month      = date('n');
        $dayOfWeek  = date('w');
        
        foreach ($this->jobs as $job) {
            $time = explode(' ', preg_replace('/\t/', ' ', $job['time']));
            
            $res = 0;
            
            /**
             * Minutes
             */
            $res += $this->checkPattern($time[0], $minute) ? 1 : 0;
            
            /**
             * Hour
             */
            $res += $this->checkPattern($time[1], $hour) ? 1 : 0;
            
            /**
             * Day
             */
            $res += $this->checkPattern($time[2], $day) ? 1 : 0;
            
            /**
             * Month
             */
            $res += $this->checkPattern($time[3], $month) ? 1 : 0;
            
            /**
             * Day of week
             */
            $res += $this->checkPattern($time[4], $dayOfWeek) ? 1 : 0;
            
            if ($res != 5) {
                continue;
            }
            
            $jobName = $job['cron'];
            /* @var $cronJob AbstractCronJob */
            
            $this->engine->getLogger('cron')->info('Cron Create Job: ' . $jobName);
            
            /**
             * Create CronJob object
             */
            $cronJob = new $jobName($this->engine);
            
            /**
             * Run cron job
             */
            $res = $cronJob->run();
            
            $this->engine->getLogger('cron')->info('Cron Job result: ' . $res);
            
			echo $res;
            echo $job['time'] . "\t" . $job['cron'] . PHP_EOL;
        }
    }
    
    /**
     * 
     * @param string $pattern
     * @param int $value
     * @return bool
     */
    private function checkPattern($pattern, $value) {
        
        $patterns = explode(',', $pattern);
        $value += 0;
        
        foreach ($patterns as $pattern) {
            $pattern = trim($pattern);
            if ($pattern == '*') {
                return true;
            }
            
            if (is_numeric($pattern) && ((int) $pattern) == $value) {
                return true;
            }
            
            if (preg_match('/^\*\/[0-9]{1,2}$/', $pattern)) {
                $pattern = substr($pattern, 2);
                if ($value % $pattern == 0) {
                    return true;
                }
            }
        }
        
        return false;
    }
}