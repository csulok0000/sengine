<?php

/**
 * 
 * @author Tibor Csik <csulok0000@gmail.com>
 * @copyright (c) 2016, Tibor Csik
 */

namespace SEngine\Lib\Cart;

abstract class SessionCartAbstract extends CartAbstract {
    
    /**
     *
     * @var string
     */
    private $cartName = '';
    
    /**
     *
     * @var array
     */
    private $store = [];
    
    /**
     *
     * @var int
     */
    private $lastItemIndex = -1;
    
    /**
     * 
     * @param string $cartName
     */
    public function __construct($cartName = 'sessionCart', $forceCreate = false) {
        $this->cartName = $cartName;
        
        // create empty cart if not exists
        if ($forceCreate || !isset($_SESSION[$cartName])) {
            $_SESSION[$cartName] = [];
        }
        
        // set pointer to session array item
        $this->store = &$_SESSION[$cartName];
        
        end($this->store);
        $this->lastItemIndex = key($this->store);
        
    }
    
    /**
     * 
     * @param \SEngine\Lib\Cart\CartItemInterface $item
     * @return int
     */
    public function addItem(\SEngine\Lib\Cart\CartItemInterface $item) {
        $this->store[++$this->lastItemIndex] = serialize($item->setIndex($this->lastItemIndex));
        return $this->lastItemIndex;
    }
    
    /**
     * 
     * @param int $index
     * @return CartItemInterface
     */
    public function getItem($index) {
        if (!isset($this->store[$index])) {
            return null;
        }
        
        return unserialize($this->store[$index]);
    }
}