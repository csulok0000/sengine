<?php

/**
 * 
 * @author Tibor Csik <csulok0000@gmail.com>
 * @copyright (c) 2015, Tibor Csik
 */

namespace SEngine;

/**
 * 
 * @deprecated since version v2.0
 */
abstract class AbstractSort {
    
    /**
     *
     * @var string
     */
    protected $type = '';
    
    /**
     *
     * @var array
     */
    protected $orderBy = '';
    
    /**
     *
     * @var bool
     */
    protected $direction = 'ASC';
    
    /**
     *
     * @var array
     */
    protected $params = array();
    
    /**
     * 
     * @param string $type
     */
    abstract public function __construct($type, $default = '');
    
    /**
     * 
     * @param string $key
     * @param mixed $value
     */
    public function setOrder($orderBy, $direction = 'ASC') {
        $this->orderBy      = $orderBy;
        $this->direction    = strtoupper($direction) == 'DESC' ? 'DESC' : 'ASC';
    }
    
    /**
     * 
     * @param bool $direction
     */
    public function setDirection($direction) {
        $this->direction = strtoupper($direction ?? '') == 'DESC' ? 'DESC' : 'ASC';
    }

    /**
     * 
     * @return array
     */
    public function toUrlGetParams($orderBy = null) {
        
        $direction = 'ASC';
        
        if (!$orderBy || $orderBy == $this->orderBy) {
            $direction = $this->direction == 'DESC' ? 'ASC' : 'DESC';
        }
        
        $params = $this->params;
        $params['orderBy'] = $orderBy ? $orderBy : $this->orderBy;
        $params['direction'] = $direction;
        return http_build_query($params);
    }
    
    /**
     * 
     * @return string
     */
    public function getOrderBy() {
        return $this->orderBy;
    }
    
    /**
     * 
     * @return string
     */
    public function getDirection() {
        return $this->direction;
    }
    
    public function addParam($key, $value) {
        $this->params[$key] = $value;
    }
}