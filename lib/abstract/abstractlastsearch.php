<?php

/**
 * 
 * @author Tibor Csik <csulok0000@gmail.com>
 * @copyright (c) 2015, Tibor Csik
 */

namespace SEngine;

/**
 * 
 * @deprecated since version v2.0
 */
abstract class AbstractLastSearch {
    
    /**
     *
     * @var string
     */
    protected $type = '';
    
    /**
     *
     * @var array
     */
    protected $params = array();
    
    /**
     * 
     * @param string $type
     */
    abstract public function __construct($type);
    
    /**
     * 
     * @param string $key
     * @param mixed $value
     */
    public function addParam($key, $value) {
        $this->params[$key] = $value;
    }
    
    /**
     * 
     * @return array
     */
    public function getParams() {
        return $this->params;
    }
    
    /**
     * 
     * @param string $key
     * @return mixed
     */
    public function getParam($key) {
        if (!isset($this->params[$key])) {
            return null;
        }
        
        return $this->params[$key];
    }
    
    /**
     * 
     * Alias for addParam
     * 
     * @param string $key
     * @param mixed $value
     */
    public function setParam($key, $value) {
        $this->addParam($key, $value);
    }
    
    /**
     * 
     * @return array
     */
    public function toUrlGetParams() {
        return http_build_query($this->params);
    }
    
    /**
     * 
     * Alias for toUrlGetParams
     * 
     * @return array
     */
    public function getQueryString() {
        return $this->toUrlGetParams();
    }
}