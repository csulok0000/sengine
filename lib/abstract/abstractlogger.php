<?php

/**
 * 
 * @author Tibor Csik <csulok0000@gmail.com>
 * @copyright (c) 2015, Tibor Csik
 */

namespace SEngine;

/**
 * 
 * @deprecated since version v2.0
 */
abstract class AbstractLogger implements LoggerInterface {
    
    /**
     * Log types
     */
    const TYPE_INFO     = 'INFO';
    const TYPE_DEBUG    = 'DEBUG';
    const TYPE_WARN     = 'WARN';
    const TYPE_ERROR    = 'ERROR';
    const TYPE_FATAL    = 'FATAL';
    
    /**
     *
     * @var bool
     */
    static public $enabled = true;
    
    /**
     *
     * @var string
     */
    protected $session = '';
 
    /**
     * 
     * @param string $logDir
     */
    public function __construct() {
        
    }
    
    /**
     * 
     * @param string $message
     * @param string $type
     */
    abstract protected function log($message, $type = self::TYPE_INFO);
    
    /**
     * 
     * @param string $message
     */
    public function debug($message) {
        if (!self::$enabled) {
            return $this;
        }
        
        if (is_array($message)) {
            $message = json_encode($message);
        }
        
        if (is_object($message)) {
            $message = var_export($message, true);
        }
        
        $this->log($message, self::TYPE_DEBUG);
        return $this;
    }
    
    /**
     * 
     * @param string $message
     */
    public function warn($message) {
        if (!self::$enabled) {
            return $this;
        }
        $this->log($message, self::TYPE_WARN);
        return $this;
    }
    
    /**
     * 
     * @param string $message
     */
    public function error($message) {
        if (!self::$enabled) {
            return $this;
        }
        $this->log($message, self::TYPE_ERROR);
        return $this;
    }
    
    /**
     * 
     * @param string $message
     */
    public function fatal($message) {
        if (!self::$enabled) {
            return $this;
        }
        $this->log($message, self::TYPE_FATAL);
        return $this;
    }
    
    /**
     * 
     * @param string $message
     */
    public function info($message) {
        if (!self::$enabled) {
            return $this;
        }
        $this->log($message, self::TYPE_INFO);
        return $this;
    }
    
    /**
     * 
     * @return string
     */
    protected function getSession() {
        if (!$this->session) {
            $this->session = strtoupper(substr(sha1(uniqid()), 0, 9));
        }
        return $this->session;
    }
}