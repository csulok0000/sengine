<?php

/**
 * 
 * @author Tibor Csik <csulok0000@gmail.com>
 * @copyright (c) 2016, Tibor Csik
 */

namespace SEngine\Lib\Cart;

abstract class CartAbstract implements CartInterface {
    
    /**
     * @param CartItemInterface $item
     */
    abstract public function addItem(CartItemInterface $item);
    
    /**
     * @return CartItemInterface
     */
    abstract public function getItem($index);
}