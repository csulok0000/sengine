<?php

/**
 * 
 * @author Tibor Csik <csulok0000@gmail.com>
 * @copyright 2015 (c), Tibor Csik
 */

namespace SEngine\Lib;

use \DaoHelper;
use \ModuleHelper;

abstract class ModuleAbstract implements \SEngine\ModuleInterface {
    
    /**
     *
     * @var \SEngine\EngineInterface
     */
    protected $engine = null;
    
    /**
     * 
     * @param \SEngine\EngineInterface $engine
     */
    public function __construct(\SEngine\EngineInterface $engine) {
        $this->engine = $engine;
    }
    
    /**
     * 
     * @param string $template
     * @param array $params
     * @return Response
     */
    public function render($template, $params = array()) {
        return $this->engine->render($template, $params);
    }
    
    /**
     * 
     * @return \SEngine\ClassLoaderInterface
     */
    public function getClassLoader() {
        return $this->getEngine()->getClassLoader();
    }
    
    /**
     * 
     * @return \Pdo
     */
    public function getDatabase() {
        return $this->engine->getDatabase();
    }
    
    /**
     * 
     * Alias for getConfigParam
     * 
     * @param string $key
     * @return mixed
     */
    public function config($key) {
        return $this->getConfigParam($key);
    }
    
    /**
     * 
     * @return DaoHelper
     */
    public function dao() {
        return $this->engine->dao();
    }
    
    /**
     * 
     * @return DaoHelper
     */
    public function getDaoHelper() {
        return $this->engine->getDaoHelper();
    }
    
    /**
     * 
     * @return ModuleHelper
     */
    public function getModuleHelper() {
        return $this->engine->getModuleHelper();
    }
    
    /**
     * 
     * @return FileCache
     */
    public function getCacheObject() {
        return $this->engine->getCacheObject();
    }
    
    /**
     * 
     * @return \SEngine\EngineInterface
     */
    public function getEngine() {
        return $this->engine;
    }
    
    /**
     * 
     * @param string $message
     */
    public function addError($message) {
        $this->getEngine()->addError($message);
    }
    
    /**
     * 
     * Return \SEngine\Response
     */
    public function redirect($url) {
        $resp = new \SEngine\Response();
        $resp->redirect($url);
        return $resp;
    }
    
    /**
     * 
     * Return \SEngine\Response
     */
    public function redirectRoute($routeName, $params = null, $extra = '') {
        return $this->redirect($this->getRouter()->getUrl($routeName, $params) . $extra);
    }
    
    /**
     * 
     * @return \SEngine\Router
     */
    public function getRouter() {
        return $this->getEngine()->getRouter();
    }
    
    /**
     * 
     * @param string $key values: key | key->subkey
     * @return mixed
     */
    public function getConfigParam($key) {
        return $this->getEngine()->getConfig()->get($key);
    }
    
    /**
     * 
     * @return \SEngine\Security
     */
    public function getSecurity() {
        return $this->getEngine()->getSecurity();
    }
    
    /**
     * 
     * @param string $type
     * @param bool $force
     * @return \SEngine\Logger
     */
    public function getLogger($type = 'default', $force = false) {
        return $this->getEngine()->getLogger($type, $force);
    }
    
    /**
     * 
     * @return array
     */
    public function getErrors() {
        return $this->getEngine()->getErrors();
    }
    
    /**
     * 
     * @return \SEngine\Request
     */
    public function getRequest() {
        return $this->getEngine()->getRequest();
    }
    
    /**
     * 
     * @param string $permissions
     * @return bool
     */
    public function hasPermission($permissions) {
        return $this->getEngine()->getRbac()->hasPermission($permissions);
    }
    
    /**
     * 
     * @param string $roles
     * @return bool
     */
    public function hasRole($roles) {
        return $this->getEngine()->getRbac()->hasRole($roles);
    }
    
    /**
     * 
     * @return \SEngine\Seo
     */
    public function getSeo() {
        return $this->getEngine()->getSeo();
    }
    
    /**
     * 
     * @param string $name
     * @param array $params
     * @param boolean $full_url
     * @return string
     */
    public function getUrl($name, $params = null, $full_url = false) {
        return $this->getRouter()->getUrl($name, $params, $full_url);
    }
    
    /**
     * 
     * @return \SEngine\Auth
     */
    public function getAuth() {
        return $this->getEngine()->getAuth();
    }
    
    /**
     * 
     * @return \SEngine\BreadCrumbs
     */
    public function getBreadCrumbs() {
        return $this->engine->getBreadCrumbs();
    }
    
}