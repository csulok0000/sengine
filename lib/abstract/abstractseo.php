<?php

/**
 * 
 * @author Tibor Csik <csulok0000@gmail.com>
 * @copyright 2015 (c), Tibor Csik
 */

namespace SEngine;

/**
 * 
 * @deprecated since version v2.0
 */
abstract class AbstractSeo implements SeoInterface {
    
    /**
     *
     * @var string
     */
    protected $title;
    
    /**
     *
     * @var string
     */
    protected $metaDescription;
    
    /**
     *
     * @var string
     */
    protected $metaKeywords;
    
    /**
     *
     * @var string
     */
    protected $canonicalUrl;
    
    /**
     * 
     * @param string $title
     */
    public function setTitle($title) {
        $this->title = $title;
    }
    
    /**
     * 
     * @param string $description
     */
    public function setMetaDescription($description) {
        $this->metaDescription = $description;
    }
    
    /**
     * 
     * @param string $keywords
     */
    public function setMetaKeywords($keywords) {
        $this->metaKeywords = $keywords;
    }
    
    /**
     * 
     * @param type $canonicalUrl
     */
    public function setCanonicalUrl($canonicalUrl) {
        $this->canonicalUrl = $canonicalUrl;
    }

    /**
     * 
     * @return string
     */
    public function getTitle() {
        return $this->title;
    }
    
    /**
     * 
     * @return string
     */
    public function getMetaDescription() {
        return $this->metaDescription;
    }
    
    /**
     * 
     * @return string
     */
    public function getMetaKeywords() {
        return $this->metaKeywords;
    }
    
    /**
     * 
     * @return string
     */
    public function getCanonicalUrl() {
        return $this->canonicalUrl;
    }
}