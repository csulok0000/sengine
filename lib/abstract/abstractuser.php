<?php

/**
 * 
 * @author Tibor Csik <csulok0000@gmail.com>
 * @copyright (c) 2016, Tibor Csik
 */

namespace SEngine;

/**
 * 
 * @deprecated since version v2.0
 */
abstract class AbstractUser implements UserInterface {
    
    /**
     *
     * @var int
     */
    protected $id;
    
    /**
     * 
     * @param int $id
     */
    public function setId($id) {
        $this->id = $id;
    }
    
    /**
     * 
     * @return int
     */
    public function getId() {
        return $this->id;
    }
}