<?php

/**
 * 
 * @author Tibor Csik <csulok0000@gmail.com>
 * @copyright (c) 2015, Tibor Csik
 */

namespace SEngine;

/**
 * 
 * @deprecated since version v2.0
 */
abstract class AbstractEntity {
 
    /**
     * 
     * @param array $arr
     */
    public function setFromArray(array $arr) {
        
        foreach ($arr as $key => $value) {
            /**
             * Create array from the underscore separated key
             */
            $keys = explode('_', $key);
            
            /**
             * set Lowercase the first part
             */
            $varName = strtolower($keys[0]);
            unset($keys[0]);
            
            foreach ($keys as $part) {
                /**
                 * Add next parts with uppercase first letter, other lowercase
                 */
                $varName .= ucfirst(strtolower($part));
            }
            
            /**
             * if exists class variables then set the value
             */
            if (isset($this->{$varName})) {
                $this->{$varName} = $value;
            }
        }
        
        return $this;
    }
    
}