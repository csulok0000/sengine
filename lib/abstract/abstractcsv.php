<?php

/**
 * 
 * @author Tibor Csik <csulok0000@gmail.com>
 * @copyright (c) 2016, Tibor Csik
 */

namespace SEngine;

/**
 * 
 * @deprecated since version v2.0
 */
abstract class AbstractCSV {
    
    const LINE_ENDING_LF = "\n";
    const LINE_ENDING_CRLF = "\r\n";
    
    /**
     *
     * @var array
     */
    protected $data = [];
    
    /**
     * 
     * @param string $delimiter
     * @param string $enclosure
     * @return string
     * @see https://css-tricks.com/snippets/php/generate-csv-from-array/
     */
    public function __toCSVString($delimiter = ',', $enclosure = '"', $lineEnding = self::LINE_ENDING_LF) {
       $csv = [];
       foreach ($this->data as $line) {
            $handle = fopen('php://temp', 'r+');
            
            // Write row
            fputcsv($handle, $line, $delimiter, $enclosure);
            rewind($handle);
            
            // Read row
            $tmp = '';
            while (!feof($handle)) {
                $tmp .= fread($handle, 8192);
            }
            
            fclose($handle);
            
            $csv[] = trim($tmp, "\r\n");
       }
       
       return implode($lineEnding, $csv);
    }
}