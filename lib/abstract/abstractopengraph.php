<?php

/**
 * 
 * @author Tibor Csik <csulok0000@gmail.com>
 * @copyright 2015 (c), Tibor Csik
 */

namespace SEngine;

/**
 * 
 * @deprecated since version v2.0
 */
abstract class AbstractOpenGraph implements OpenGraphInterface {
    
    /**
     *
     * @var string
     */
    protected $ogTitle;
    
    /**
     *
     * @var string
     */
    protected $ogDescription;
    
    /**
     *
     * @var string
     */
    protected $ogType;
    
    /**
     *
     * @var string
     */
    protected $ogImage;
    
    /**
     *
     * @var string
     */
    protected $ogUrl;
    
    /**
     * 
     * @param string $ogTitle
     */
    public function setOgTitle($ogTitle) {
        $this->ogTitle = $ogTitle;
    }
    
    /**
     * 
     * @param string $ogDescription
     */
    public function setOgDescription($ogDescription) {
        $this->ogDescription = $ogDescription;
    }
    
    /**
     * 
     * @param string $ogType
     */
    public function setOgType($ogType) {
        $this->ogType = $ogType;
    }
    
    /**
     * 
     * @param string $ogImage
     */
    public function setOgImage($ogImage) {
        $this->ogImage = $ogImage;
    }
    
    /**
     * 
     * @param string $ogUrl
     */
    public function setOgUrl($ogUrl) {
        $this->ogUrl = $ogUrl;
    }
    
    /**
     * 
     * @return string
     */
    public function getOgTitle() {
        return $this->ogTitle;
    }
    
    /**
     * 
     * @return string
     */
    public function getOgDescription() {
        return $this->ogDescription;
    }
    
    /**
     * 
     * @return string
     */
    public function getOgType() {
        return $this->ogType;
    }
    
    /**
     * 
     * @return string
     */
    public function getOgImage() {
        return $this->ogImage;
    }
    
    /**
     * 
     * @return string
     */
    public function getOgUrl() {
        return $this->ogUrl;
    }
}