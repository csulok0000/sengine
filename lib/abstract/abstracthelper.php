<?php

/**
 * 
 * @author Tibor Csik <csulok0000@gmail.com>
 * @copyright 2015 (c), Tibor Csik
 */

namespace SEngine;

/**
 * 
 * @deprecated since version v2.0
 */
abstract class AbstractHelper {
    
    /**
     *
     * @var Engine
     */
    protected $engine = null;
    
    /**
     *
     * @var array
     */
    protected $container = array();
    
    /**
     * 
     * @param Engine $engine
     */
    final public function __construct(EngineInterface $engine) {
        $this->engine = $engine;
    }
    
    abstract protected function get($name);
}