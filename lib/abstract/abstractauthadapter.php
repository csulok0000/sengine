<?php

/**
 * 
 * @author Tibor Csik <csulok0000@gmail.com>
 * @copyright (c) 2015, Tibor Csik
 */

namespace SEngine;

/**
 * 
 * @deprecated since version v2.0
 */
abstract class AbstractAuthAdapter implements AuthAdapterInterface {
    
    /**
     *
     * @var EngineInterface
     */
    protected $engine;
    
    /**
     * 
     * @param EngineInterface $engine
     */
    public function __construct(EngineInterface $engine) {
        $this->engine = $engine;
    }
    
    /**
     * 
     * @return EngineInterface
     */
    public function getEngine() {
        return $this->engine;
    }
    
    /**
     * Reload logged user data, if needed.
     */
    public function load() {
        ;
    }
}