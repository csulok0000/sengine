<?php

/**
 * 
 * @author Tibor Csik <csulok0000@gmail.com>
 * @copyright (c) 2016, Tibor Csik
 */

namespace SEngine\Lib\Cart;

abstract class CartItemAbstract implements CartItemInterface {
    
    /**
     *
     * @var int
     */
    protected $index = 0;
    
    /**
     *
     * @var int
     */
    protected $quantity = 1;
    
    /**
     * @param int $index
     * @return CartItemAbstract
     */
    public function setIndex($index) {
        $this->index = $index;
        return $this;
    }
    
    /**
     * 
     * @return int
     */
    public function getIndex() {
        return $this->index;
    }
    
    /**
     * 
     * @param int $quantity
     * @return \SEngine\Lib\Cart\CartItemAbstract
     */
    public function setQuantity($quantity) {
        $this->quantity = $quantity;
        return $this;
    }
}