<?php

/**
 * 
 * @author Tibor Csik <csulok0000@gmail.com>
 * @copyright 2015 (c), Tibor Csik
 */

namespace SEngine\Lib;

abstract class EngineAbstract implements EngineInterface {
    
    /**
     *
     * @var AbstractAuth
     */
    protected $auth;
    
    /**
     *
     * @var ClassLoaderInterface
     */
    protected $classLoader;
    
    /**
     *
     * @var \Pdo
     */
    protected $db;
    
    /**
     *
     * @var View
     */
    protected $view;
    
    /**
     *
     * @var Router
     */
    protected $router;
    
    /**
     *
     * @var Array
     */
    protected $config;
    
    /**
     *
     * @var DaoHelper
     */
    protected $daoHelper;
    
    /**
     *
     * @var ModuleHelper
     */
    protected $moduleHelper;
    
    /**
     *
     * @var FileCache
     */
    protected $cache;
    
    /**
     *
     * @var Seo
     */
    protected $seo;
    
    /**
     *
     * @var Security
     */
    protected $security;
    
    /**
     *
     * @var array 
     */
    protected $logger = array();
    
    /**
     *
     * @var Rbac
     */
    protected $rbac;
    
    /**
     *
     * @var \SEngine\Request
     */
    protected $request;
    
    /**
     *
     * @var \SEngine\HookInterface
     */
    protected $hook;
    
    /**
     *
     * @var string
     */
    public $logDir = '';
            
    /**
     *
     * @var array 
     */
    public $errors = array();
    
    /**
     *
     * @var BreadCrumbs
     */
    protected $breadCrumbs;
    
    /**
     * 
     */
    public function __construct() {
        
    }
    
    /**
     * 
     * @param string $key
     * @return mixed
     */
    public function config($key) {
        return $this->getConfig()->get($key);
    }
    
    /**
     * 
     * @return \SEngine\AuthInterface
     */
    public function getAuth() {
        return $this->auth;
    }
    
    /**
     * 
     * @param \SEngine\AuthInterface $auth
     * @return $this
     */
    public function setAuth(\SEngine\AuthInterface $auth) {
        $this->auth = $auth;
        return $this;
    }
    
    /**
     * 
     * @return BreadCrumbs
     */
    public function getBreadCrumbs() {
        if (!$this->breadCrumbs) {
            $this->breadCrumbs = new BreadCrumbs();
        }
        
        return $this->breadCrumbs;
    }
    
    /**
     * 
     * @return type
     */
    public function getClassLoader() {
        return $this->classLoader;
    }
    
    /**
     * 
     * @param \Pdo $db
     */
    public function setDatabase(\Pdo $db) {
        $this->db = $db;
    }
    
    /**
     * 
     * @return \Pdo
     */
    public function getDatabase() {
        return $this->db;
    }
    
    /**
     * 
     * @return AbstractView
     */
    public function getView() {
        return $this->view;
    }
    
    /**
     * 
     * @param string $template
     * @param array $params
     * @return Response
     */
    public function render($template, $params) {
        return $this->getView()->render($template, $params);
    }
    
    /**
     * 
     * @param Config $config
     */
    public function setConfig(Config $config) {
        $this->config = $config;
    }
    
    /**
     * 
     * @return Config
     */
    public function getConfig() {
        if (!$this->config) {
            throw new Exception('Config is not set!');
        }
        return $this->config;
    }
    
    /**
     * 
     * @return Router
     */
    public function getRouter() {
       return $this->router; 
    }
    
    /**
     * 
     * @return DaoHelperInterface
     */
    public function dao() {
        if (!$this->daoHelper) {
            $this->daoHelper = new \DaoHelper($this);
        }
        return $this->daoHelper;
    }
    
    /**
     * 
     * @return DaoHelperInterface
     */
    public function getDaoHelper() {
        return $this->dao();
    }
    
    /**
     * 
     * @return ModuleHelper
     */
    public function getModuleHelper() {
        if (!$this->moduleHelper) {
            $this->moduleHelper = new \ModuleHelper($this);
        }
        return $this->moduleHelper;
    }
    
    /**
     * 
     * @param string $message
     */
    public function addError($message) {
        $this->errors[] = $message;
    }
    
    /**
     * 
     * @return array
     */
    public function getErrors() {
        return $this->errors;
    }
    
    /**
     * 
     * @param Router $router
     */
    public function setRouter(Router $router) {
        $this->router = $router;
    }
    
    /**
     * 
     * @return Seo
     */
    public function getSeo() {
        if (!$this->seo) {
            $this->seo = new Seo();
        }
        
        return $this->seo;
    }
    
    /**
     * 
     * @return Security
     */
    public function getSecurity() {
        if (!$this->security) {
            $this->security = new Security();
        }
        return $this->security;
    }
    
    /**
     * 
     * @return Rbac
     */
    public function getRbac() {
        return $this->rbac;
    }
    
    /**
     * 
     * @param Rbac $rbac
     */
    public function setRbac(Rbac $rbac) {
        $this->rbac = $rbac;
    }
    
    /**
     * 
     * @return Request
     */
    public function getRequest() {
        if (!$this->request) {
            $this->request = new Request();
        }
        
        return $this->request;
    }
    
    /**
     * 
     * @return \SEngine\HookInterface
     */
    public function getHook() {
        return $this->hook;
    }
    
    /**
     * 
     * @param \SEngine\HookInterface $hook
     * @return $this
     */
    public function setHook(\SEngine\HookInterface $hook) {
        $this->hook = $hook;
        return $this;
    }
    
    /**
     * 
     * @param string $routeFile
     */
    abstract public function run($routeFile = '');
    
    /**
     * 
     * @param string $type
     * @param bool $force
     */
    public function getLogger($type = 'default', $force = false) {
        if ($force || !isset($this->logger[$type])) {
            if ($type == 'default') {
                $class = __NAMESPACE__ . '\\Logger';
            } else {
                $class = '\\' . $type . 'Logger';
            }
            if (!class_exists($class)) {
                $class = __NAMESPACE__ . '\\Logger';
            }

            $this->logger[$type] = new $class(rtrim($this->logDir, '/') . '/', $type == 'default' ? '' : $type);
        }
        
        return $this->logger[$type];
    }
    
    /**
     * 
     * @param AbstractCache $cache
     */
    public function setCacheObject(AbstractCache $cache) {
        $this->cache = $cache;
    }
    
    /**
     * 
     * @return AbstractCache
     */
    public function getCacheObject() {
        return $this->cache;
    }
    
    /**
     * 
     * @param \SEngine\AbstractView $view
     */
    public function setView(AbstractView $view) {
        $this->view = $view;
    }
    
    public function setClassLoader(ClassLoaderInterface $loader) {
        $this->classLoader = $loader;
    }
}

