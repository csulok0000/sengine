<?php

/**
 * 
 * @author Tibor Csik <csulok0000@gmail.com>
 * @copyright (c) 2016, Tibor Csik
 */

/**
 * 
 * @deprecated since version v2.0
 */
abstract class AbstractFile {
    
    /**
     *
     * A projekt gyökénykönyvtár számára
     * Az mkdir metódus nem próbál meg mappát létrehozni a gyökénykönyvtár felet
     * 
     * @var string
     */
    protected $rootPath = '/';
    
    /**
     * 
     * @param string $dir
     * @param int $perm
     */
    public function mkdir($dir, $perm = 0644) {
        if ($dir[0] != '/') {
            $dir = getcwd() . '/' . $dir;
        }
        
        $root = '';
        //$dir = str_replace($this->rootPath, '', $dir, 1);
        if ($this->rootPath && strpos($dir, $this->rootPath) === 0) {
            $root = $this->rootPath;
            $dir = substr($dir, strlen($root));
        }
        
        $dirs = explode('/', trim($dir, '/'));
        $path = '';
        
        foreach ($dirs as $dir) {
            $path .= '/' . $dir;
            if (!is_dir($root . $path)) {
                mkdir($root . $path);
                chmod($root . $path, $perm);
            }
        }
    }
    
    /**
     * 
     * @param array $image $_FILES['file']
     * @return bool
     */
    public function isValidPostFile($file) {
        return !$file['error'] && $file['size'] && file_exists($file['tmp_name']);
    }
}