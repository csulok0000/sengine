<?php

/**
 * 
 * @author Tibor Csik <csulok0000@gmail.com>
 * @copyright 2015 (c), Tibor Csik
 */

namespace SEngine\Lib;

use \Pdo;

abstract class DaoAbstract implements DaoInterface {
    
    /**
     *
     * @var Pdo
     */
    protected $db;
    
    /**
     * 
     * @param Pdo $db
     */
    final public function __construct(\Pdo $db) {
        $this->db = $db;
    }
    
    /**
     * 
     * @return \PDO
     */
    final protected function db() {
        return $this->db;
    }
    
    /**
     * 
     * @return Pdo
     * @deprecated since version v2.0
     */
    final protected function getDb() {
        return $this->db();
    }
}