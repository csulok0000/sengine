<?php

/**
 * 
 * @author Tibor Csik <csulok0000@gmail.com>
 * @copyright 2015 (c), Tibor Csik
 */

namespace SEngine;

/**
 * 
 * @deprecated since version v2.0
 */
abstract class AbstractCache implements CacheInterface {
    
    /**
     * 
     * @param string $key
     * @return mixed
     */
    abstract public function get($key);
    
    /**
     * 
     * @param string $key
     * @param mixed $data
     * @param int $time
     */
    abstract public function set($key, $data, $time = 600);
    
    /**
     * @param string $key
     */
    abstract public function delete($key);
    
    /**
     * 
     * @param string $text
     * @param int $length A kulcs maximális hossza, a függvény a kulcs végéhez kapcsol egy 4 karakteres hash stringet
     * @return string
     */
    public function generateKey($data, $length = 60) {
        if (!is_array($data)) {
            $data = [$data];
        }
        
        $key = '';
        
        if ($length < 10) {
            throw new Exception('A kulcs maximális hossza túl rövid: ' . $length . '. Minimum 10 karakter legyen!');
        }
        // A végére + 4 karatert hozzá fűzünk
        $length -= 4;
        
        $count = count($data);
        $rowSize = floor($length / $count);
        
        if ($rowSize < 1) {
            $rowSize = 1;
        }
        
        foreach ($data as $row) {
            $key .= substr(preg_replace('/[^0-9a-z]/i', '', strtolower($row ?? '')), 0, $rowSize);
        }
        
        $key .= substr(md5(implode($data)), 0, 4);
        
        return $key;
    }
}