<?php

/**
 * 
 * @author Tibor Csik <csulok0000@gmail.com>
 * @copyright (c) 2015, Tibor Csik
 */

namespace SEngine\Lib;

abstract class AuthAbstract implements \SEngine\AuthInterface {
    
    /**
     *
     * @var \SEngine\AuthAdapterInterface
     */
    protected $authAdapter;
    
    /**
     * 
     * @param \SEngine\AuthAdapterInterface $adapter
     */
    public function setAuthAdapter(\SEngine\AuthAdapterInterface $adapter) {
        $this->authAdapter = $adapter;
    }
    
    /**
     * 
     * @param string $username
     * @param string $password
     * @param string $type
     * @return mixed
     */
    public function auth($username, $password, $type = 'user') {
        return $this->authAdapter->auth($username, $password, $type);
    }
    
    /**
     * Call authadapterInterface::load
     */
    public function init() {
        $this->authAdapter->load();
    }
    
    /**
     * 
     * @return \SEngine\UserInterface
     */
    public function get() {
        return $this->authAdapter->get();
    }
    
    /**
     * 
     * @param mixed $user
     */
    public function set($user) {
        $this->authAdapter->set($user);
    }
    
    /**
     * 
     * @return int
     */
    public function getId() {
        return $this->get()->getId();
    }
    
    /**
     * 
     * @param int $id
     */
    public function setId($id) {
        $this->get()->setId($id);
    }
}