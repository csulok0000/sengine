<?php

/**
 * 
 * @author Tibor Csik <csulok0000@gmail.com>
 * @copyright 2015 (c), Tibor Csik
 */

namespace SEngine;

use Exception;

class Pagination {
    
    /**
     *
     * The number of all items
     * 
     * @var int
     */
    protected $itemCount = 0;
    
    /**
     *
     * The number of items/page
     * 
     * @var int
     */
    protected $pageLimit = 10;
    
    /**
     *
     * Request method type
     * 
     * Supported types: GET | ROUTE
     * 
     * @var string
     */
    protected $method = 'GET';
    
    /**
     *
     * @var int
     */
    protected $currentPage = 1;
    
    /**
     *
     * @var string
     */
    protected $url = '?';
    
    /**
     *
     * The name of page parameter in url or post data
     * 
     * @var string
     */
    protected $parameterName = 'page';
    
    protected $extraParams = array();
    
    /**
     * 
     * @param int $currentPage
     * @param int $pageLimit
     * @param string $parameterName
     */
    public function __construct($currentPage = 1, $pageLimit = 10, $parameterName = 'page') {
        $this->currentPage      = $currentPage > 0 ? (int) $currentPage : 1;
        $this->pageLimit        = (int) $pageLimit;
        $this->parameterName    = $parameterName;
    }
    
    /**
     * 
     * Set item count
     * 
     * @param int $count
     */
    public function setItemsCount($count) {
        if (!is_numeric($count)) {
            throw new Exception('Invalid item count.');
        }
        
        $this->itemCount = (int) $count;
    }
    
    /**
     * 
     * Set count of items/page
     * 
     * @param int $limit
     */
    public function setPageLimit($limit) {
        $this->pageLimit = $limit;
    }
    
    /**
     * 
     * get count of items/page
     * 
     * @return int
     */
    public function getPageLimit() {
        return $this->pageLimit;
    }
    
    /**
     * 
     * Set the pager request type GET|POST
     * 
     * @param string $method GET|POST
     */
    public function setMethod($method) {
        $this->method = strtoupper($method);
    }
    
    /**
     * 
     * @param string $url
     */
    public function setUrl($url) {
        $this->url = $url;
        
        if ($this->method == 'ROUTE') {
            return true;
        }
        
        $lastChar = substr($this->url, -1);
        
        if ($lastChar != '?' && $lastChar != '&') {
            if (strstr($this->url, '?')) {
                $this->url .= '&';
            } else {
                $this->url .= '?';
            }
        }
    }
    
    /**
     * 
     * @param string $name
     * @param mixed $value
     * @param string $method default|get|post
     */
    public function addExtraParams($name, $value) {
        $this->extraParams[$name] = $value;
    }
    
    /**
     * 
     * @param int $page
     */
    public function setCurrentPage($page) {
        $this->currentPage = $page;
    }
    
    /**
     * 
     * @return int $page
     */
    public function getCurrentPage() {
        return $this->currentPage;
    }
    
    /**
     * 
     * @param string $name
     */
    public function setParameterName($name) {
        $this->parameterName = $name;
    }
    
    /**
     * 
     * @return array
     */
    public function getPaginationArray() {
        return array(
            'pageCount'         => ceil($this->itemCount / $this->pageLimit),
            'currentPage'       => $this->currentPage,
            'url'               => $this->url,
            'method'            => $this->method,
            'extraParams'       => http_build_query($this->extraParams),
            'extraParamsArray'  => $this->extraParams,
            'parameterName'     => $this->parameterName
        );
    }
    
    /**
     * 
     * @return array
     */
    public function toArray() {
        return $this->getPaginationArray();
    }
    
}