<?php

/**
 * 
 * @author Tibor Csik <csulok0000@gmail.com>
 */

namespace SEngine\Factory;

use PDO;

class DatabaseFactory {
    
    /**
     *
     * @var \PDO[]
     */
    static private $store = [];
    
    /**
     * 
     * @param string $name
     * @param string $host
     * @param string $dbName
     * @param string $user
     * @param string $password
     * @return \PDO
     */
    static public function get($name = 'default', $host = null, $dbName = null, $user = null, $password = null) {
        if (!isset(self::$store[$name])) {
            
            if ($host && $dbName) {
                return self::create($host, $dbName, $user, $password, $name);
            } else {
                return null;
            }
        }
        
        return self::$store[$name];
    }
    
    /**
     * 
     * @param string $name
     * @param string $host
     * @param string $dbName
     * @param string $user
     * @param string $password
     * @return \PDO
     */
    static public function create($host, $dbName, $user, $password, $name = 'default') {
        self::$store[$name] = new Pdo('mysql:host=' . $host . ';dbname=' . $dbName, $user, $password);
        self::$store[$name]->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        self::$store[$name]->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC); 
        self::$store[$name]->query('SET NAMES "UTF8"');
        
        return self::$store[$name];
    }
}