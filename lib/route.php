<?php

/**
 * 
 * @author Tibor Csik <csulok0000@gmail.com>
 * @copyright 2015 (c), Tibor Csik
 */

namespace SEngine;

class Route implements \JsonSerializable {
     
   /**
     *
     * @var string
     */
    protected $name     = '';
    
    /**
     *
     * @var string
     */
    protected $module   = '';
    
    /**
     *
     * @var string
     */
    protected $method   = '';
    
    /**
     *
     * @var string
     */
    protected $template = '';
    
    /**
     *
     * @var string
     */
    protected $scope = '';
    
    /**
     *
     * @var string
     */
    protected $security = '';
    
    /**
     *
     * @var array
     */
    protected $parameters = array();
    
    /**
     * 
     * @param string $name
     * @param string $module
     * @param string $method
     * @param string $template
     * @param mixed[] $parameters
     */
    public function __construct($name, $module, $method, $template, $parameters, $scope, $security = '') {
        $this->name         = $name;
        $this->module       = $module;
        $this->method       = $method;
        $this->template     = $template;
        $this->parameters   = $parameters;
        $this->scope        = $scope ? $scope : $this->scope;
        $this->security     = $security;
    }
    
    /**
     * 
     * @return string
     */
    public function getRouteName() {
        return $this->name;
    }
    
    /**
     * 
     * @return string
     */
    public function getModule() {
        return $this->module;
    }
    
    /**
     * 
     * @return string
     */
    public function getMethod() {
        return $this->method;
    }
    
    /**
     * 
     * @return string
     */
    public function getTemplate() {
        return $this->template;
    }
    
    /**
     * 
     * @return array
     */
    public function getParameters() {
        return $this->parameters;
    }
    
    /**
     * 
     * @return string
     */
    public function getScope() {
        return $this->scope;
    }
    
    /**
     * 
     * @return string
     */
    public function getSecurity() {
        return $this->security;
    }
    
    /**
     * 
     * @return mixed
     */
    public function jsonSerialize(): array {
        return array(
            'name'          => $this->name,
            'module'        => $this->module,
            'method'        => $this->method,
            'template'      => $this->template,
            'parameters'    => $this->parameters,
            'scope'         => $this->scope,
            'security'      => $this->security
        );
    }

    
}