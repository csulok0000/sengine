<?php

/**
 * 
 * @author Tibor Csik <csulok0000@gmail.com>
 * @copyright 2015 (c), Tibor Csik
 */

namespace SEngine;

class StaticRequest {
    
    /**
     * 
     * @return bool
     */
    static public function isPost() {
        return strtoupper($_SERVER['REQUEST_METHOD']) == 'POST';
    }
    
    /**
     * 
     * @return bool
     */
    static public function isGet() {
        return strtoupper($_SERVER['REQUEST_METHOD']) == 'GET';
    }
    
    /**
     * 
     * @param string $name
     * @return mixed
     */
    static public function get($name, $trim = false) {
        /**$tmp = filter_input(INPUT_GET, $name);
        return $tmp ? $tmp : filter_input(INPUT_GET, $name, FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);*/
        //return isset($_GET[$name]) ? $_GET[$name] : null;
        return self::arrayValue($_GET, $name, $trim);
    }
    
    /**
     * 
     * @param string $name
     * @return mixed
     */
    static public function post($name, $trim = false) {
        /**$tmp = filter_input(INPUT_POST, $name);
        return $tmp ? $tmp : filter_input(INPUT_POST, $name, FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);*/
        //return isset($_POST[$name]) ? $_POST[$name] : null;
        return self::arrayValue($_POST, $name, $trim);
    }
    
    /**
     * 
     * @param string $name
     * @return mixed
     */
    static public function cookie($name, $trim = false) {
        //return filter_input(INPUT_COOKIE, $name);
        //return isset($_COOKIE[$name]) ? $_COOKIE[$name] : null;
        return self::arrayValue($_COOKIE, $name, $trim);
    }
    
    /**
     * 
     * @param string $name
     * @return mixed
     */
    static public function server($name, $trim = false) {
        //return filter_input(INPUT_SERVER, $name);
        //return isset($_SERVER[$name]) ? $_SERVER[$name] : null;
        return self::arrayValue($_SERVER, $name, $trim);
    }
    
    /**
     * 
     * @param string $name
     * @return mixed
     */
    static public function session($name, $trim = false) {
        // Not implemented yet ( PHP 5.5? <)
        //return filter_input(INPUT_SESSION, $name);
        //return isset($_SESSION[$name]) ? $_SESSION[$name] : null;
        if (!isset($_SESSION)) {
            return null;
        }
        
        return self::arrayValue($_SESSION, $name, $trim);
    }
    
    /**
     * 
     * @param string $path pl. testKey, site->url
     * @return mixed|null
     */
    static public function request($path) {
        return self::arrayValue($_REQUEST, $path);
    }
    
    /**
     * 
     * @param string $name
     * @return mixed
     */
    static public function file($name) {
        $res = isset($_FILES[$name]) ? $_FILES[$name] : null;
        
        if ($res && is_array($res['name'])) {
            $fileCount = count($res['name']);
            $fileKeys = array_keys($res);
            $tmp = array();

            for ($i = 0; $i < $fileCount; $i++) {
                foreach ($fileKeys as $key) {
                    $tmp[$i][$key] = $res[$key][$i];
                }
            }
            $res = $tmp;
        }
        
        return $res;
    }
    
    /**
     * 
     * @param array $file $_FILES['file']
     * @return bool
     */
    static public function isValidPostFile($file) {
        return !$file['error'] && $file['size'] && file_exists($file['tmp_name']);
    }
    
    /**
     * 
     * @param array $array
     * @param string $path ex.: site, site->host
     * @return type
     */
    static public function arrayValue($array, $path, $trim = false) {
        if ($path === '') {
            return null;
        }
        
        $keys = explode('->', $path);
        $search = $array;
        foreach ($keys as $key) {
            if (!is_array($search) || !$search) {
                    return null;
            }
            if (!array_key_exists($key, $search)) {
                    return null;
            }
            $search = $search[$key];
        }
        return is_scalar($search) && $trim ? trim($search) : $search;

    }
}