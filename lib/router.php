<?php

/**
 * 
 * @author Tibor Csik <csulok0000@gmail.com>
 * @copyright 2015 (c), Tibor Csik
 */

namespace SEngine;

class Router {
    
    /**
     *
     * @var array
     */
    protected $routeTable = array();
    
    /**
     *
     * @var string
     */
    protected $request;
    
    /**
     *
     * @var Route
     */
    protected $route = null;
    
    /**
     *
     * @var string
     */
    protected $host = '';
    
    /**
     * 
     * @param array $routeTable
     * @param string $request
     */
    public function __construct($routeTable, $request = '', $host = '') {
        $this->routeTable = $routeTable;
        
        $this->request = strpos($request, '?') ? substr($request, 0, strpos($request, '?')) : $request;
        
        $this->host = $host;
        
        // Pricess Request
        $this->process();
    }
    
    /**
     * 
     * @throws \Exception
     */
    private function process() {
        if (!$this->routeTable) {
            return;
        }
        
        foreach ($this->routeTable as $name => $route) {
            //
            // A perjeleket átalakítjuk, hogy ne kifejezésként kezelje
            //
            $route['pattern'] = str_replace('/', "\\/", $route['pattern']);
            
            //
            // A végén lévő per jel elhagyható
            //
            if ($route['pattern'][strlen($route['pattern']) - 1] == '/') {
                $route['pattern'] .= '?';
            }
            
            //Paraméterek kezelése
            $params = array();
            if (preg_match_all("/\{[^\}]*\}/i", $route['pattern'], $params)) {
                foreach ($params as $param) {
                    $route['pattern'] = str_replace($param, '([^\/]*)', $route['pattern']);
                }
            }
            
            if (preg_match_all("/^" . $route['pattern'] . "$/i", $this->request, $match)) {
					
                $control = $action = $template = $scope = $security = '';
					
                $parameters = array();
                if (array_key_exists('target', $route)) {
                    //
                    // Target
                    //
                    $target = explode(':', $route['target']);
                    
                    if (count($target) === 3) {
                        $route['scope'] = $target[0];
                        $control        = $target[1];
                        $action         = $target[2];
                    } else {
                        list($control, $action) = $target;
                    }
                    
                    if (count($match) > 1) {
                        foreach ($match as $index => $p) {
                            if ($index) {
                                $parameters[trim($params[0][$index-1], '{}')] = $p[0];
                            }
                        }
                    }
                } elseif (array_key_exists('template', $route)) {
                    $template = $route['template'];
                }
                
                if (isset($route['scope'])) {
                    $scope = $route['scope'];
                }
                
                if (isset($route['security'])) {
                    $security = $route['security'];
                }

                $this->route = new Route($name, $control, $action, $template, $parameters, $scope, $security);
                break;
            }
        }
    }
    
    /**
     * 
     * @return Route
     */
    public function getRoute() {
        return $this->route;
    }
    
    /**
     * 
     * @param string $routeName
     * @param array $parameters
     * @return Route
     * @throws RouteNotFoundException
     */
    public function getRouteByRouteName($routeName, $parameters = array()) {
        
        if (!isset($this->routeTable[$routeName])) {
            throw new RouteNotFoundException($routeName);
        }
        
        $control = $action = $template = $scope = $security = '';
			
        $route = $this->routeTable[$routeName];
        
        if (array_key_exists('target', $route)) {
            //
            // Target
            //
            $target = explode(':', $route['target']);
            if (count($target) === 3) {
                $route['scope'] = $target[0];
                $control        = $target[1];
                $action         = $target[2];
            } else {
                list($control, $action) = $target;
            }
        }
        
        if (array_key_exists('template', $route)) {
            $template = $route['template'];
        }

        if (isset($route['scope'])) {
            $scope = $route['scope'];
        }

        if (isset($route['security'])) {
            $security = $route['security'];
        }

        return new Route($routeName, $control, $action, $template, $parameters, $scope, $security);
    }
    
    /**
     *
     * getUrl
     *
     * @param string $name Name in the routing table
     * @param mixed $params
     * @param bool $full_url return the full url: http://example.com/...
     * @return srting
     *
     */
    public function getUrl($name, $params = null, $full_url = false) {
        if (array_key_exists($name, $this->routeTable)) {
            $pattern = $this->routeTable[$name]['pattern'];
            //Változók ellenőrzése
            if (preg_match_all("/\{([^\}]*)\}/i", $pattern, $match)) {
                if (array_key_exists(1, $match)) {
                    foreach ($match[1] as $variable) {
                        if (array_key_exists($variable, $params)) {
                            $pattern = str_replace('{' . $variable . '}', $params[$variable], $pattern);
                        } else {
                            trigger_error('Undefined variable: ' . $variable, E_USER_NOTICE);
                            return false;
                        }
                    }
                }
            }
            
            if ($full_url) {
                return $this->host . $pattern;
            } else {
                return $pattern;
            }
        } else {
            trigger_error('Routing not exists: ' . $name, E_USER_NOTICE);
        }
    }
    
    /**
     * 
     * @return array
     */
    public function getRouteTable() {
        return $this->routeTable;
    }
}