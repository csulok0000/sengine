<?php

/**
 * 
 * @author Tibor Csik <csulok0000@gmail.com>
 * @copyright 2015 (c), Tibor Csik
 */

namespace SEngine;

class Config implements ConfigInterface {
    
    /**
     *
     * @var array
     */
    protected $raw = [];
    
    /**
     *
     * @var array
     */
    protected $config = [];
    
    /**
     *
     * @var string
     */
    protected $suffix = '';
    
    /**
     * 
     * @param array $config
     */
    public function __construct($config, $suffix = '') {
        $this->raw = $config;
        
        $this->suffix = $suffix ? ':' . trim($suffix, ':') : '';
        
        $this->config = $this->processRawConfig();
    }
    
    /**
     * 
     * @param string $path "site->key"
     * @return mixed
     */
    public function get($path) {
        $keys = explode('->', $path);
        
        if ($path) {
            $search = $this->config;
            foreach ($keys as $key) {
                if (!is_array($search) || !$search) {
                        return null;
                }
                if (!array_key_exists($key, $search)) {
                        return null;
                }
                $search = $search[$key];
            }
            return $search;
        }
        return null;
    }
    
    /**
     * 
     * @param string $path
     * @param mixed $value
     */
    public function set($path, $value) {
        $keys = explode('->', $path);
        
        if ($keys) {
            $search = &$this->config;
            $length = count($keys);
            foreach ($keys as $index => $key) {
                if ($index == $length -1) {
                    $search[$key] = $value;
                } elseif (!isset($search[$key])) {
                    $search[$key] = array();
                }
                
                $search = &$search[$key];
            }
        }
    }
    
    /**
     * Return array
     */
    public function getConfigArray() {
        return $this->config;
    }
    
    private function ProcessRawConfig() {
        $config = array();
        $current = array();
        
        if (isset($this->raw['loadConfig'])) {
            
            $loaded = [];
            foreach ($this->raw['loadConfig'] as $key => $value) {
                $file = $value[0] == '/' ? $value : CONFIG_DIR . '/' . $value;
                
                if (file_exists($file)) {
                    $tmp = parse_ini_file($file, true);
                    $loaded = array_replace_recursive($loaded, $tmp);
                }
            }
            $this->raw = array_replace($this->raw, $loaded);
        }
        
        foreach ($this->raw as $section => $pair) {
            /**
             * Global
             */
            if (strpos($section, ':') === false) {
                if (is_array($pair)) {
                    foreach ($pair as $key => $value) {
                        $config[$section][$key] = $value;
                    }
                } else {
                    $config[$section] = $pair;
                }
            } elseif ($this->suffix && strpos($section, $this->suffix) !== false) {
                /**
                 * Current
                 */
                $section = substr($section, 0, -strlen($this->suffix));
                if (is_array($pair)) {
                    foreach ($pair as $key => $value) {
                        $current[$section][$key] =  $value;
                    }
                } else {
                    $config[$section] = $pair;
                }
            }
        }
        
        return array_replace_recursive($config, $current);
    }
    
    public function setConfigArray(array $config) {
        $this->config = $config;
    }
    
}