<?php

/**
 * 
 * @author Tibor Csik <csulok0000@gmail.com>
 * @copyright 2015 (c), Tibor Csik
 */

namespace SEngine;

class Hook implements HookInterface {
    
    /**
     *
     * @var Engine
     */
    protected $engine = null;
    
    /**
     *
     * @var Logger
     */
    protected $logger = null;
    
    /**
     * 
     * @param Engine $engine
     * @param Logger $logger
     */
    public function __construct(EngineInterface $engine, LoggerInterface $logger = null) {
        $this->engine = $engine;
        $this->logger = $logger;
    }
    
    /**
     * 
     * @param type $hook <type>:<event> ex.: Engine:beforeRun
     * @return boolean
     */
    public function run($hook) {
        list($control, $method) = explode(':', $hook);
        
        $class = $control . 'Hook';
        
        if (!class_exists($class)) {
            if ($this->logger) {
                $this->logger->info($hook . "\t" . $class . ' not found');
            }
            return false;
        }
        
        $module = new $class($this->engine);
        
        if (!method_exists($module, $method)) {
            if ($this->logger) {
                $this->logger->info($hook . "\t" . $class . '->' . $method . ' not found');
            }
            return false;
        }
        
        $res = call_user_func(array($module, $method));
        
        if ($this->logger) {
            $this->logger->info($hook . "\t successfull. Response: " . json_encode($res));
        }
    }
    
    /**
     * 
     * @param LoggerInterface $logger
     */
    public function setLogger(LoggerInterface $logger) {
        $this->logger = $logger;
    }
}