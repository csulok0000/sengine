<?php

/**
 * 
 * @author Tibor Csik <csulok0000@gmail.com>
 */

namespace SEngine;

use JsonSerializable;

interface ResponseInterface extends JsonSerializable {
    
    /**
     * 
     * @param mixed $data
     * @param int $type
     * @throws Exception
     */
    public function __construct($data, $type);
    
    /**
     * 
     * @return string
     * @throws Exception
     */
    public function __toString();
    
    /**
     * 
     * Set the html value for response, and set response type to Response::TYPE_HTML
     * 
     * @param string $html
     */
    public function setHTML($html);
    
    /**
     * 
     * Set the json value for response, and set response type to Response::TYPE_AJAX
     * 
     * @param array $json
     */
    public function setJSON($json);
    
    /**
     * 
     * @return int
     */
    public function getType();
    
    /**
     * 
     * @param string $url
     */
    public function redirect($url);
    
    /**
     * 
     * @return string
     */
    public function getRedirectUrl();
    
    /**
     * 
     * @return string
     */
    public function jsonSerialize(): string;
    
    /**
     * 
     * @return array
     */
    public function getJSON();
}
