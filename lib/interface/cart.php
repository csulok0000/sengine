<?php

/**
 * 
 * @author Tibor Csik <csulok0000@gmail.com>
 * @copyright (c) 2016, Tibor Csik
 */

namespace SEngine\Lib\Cart;

interface CartInterface {
    
    /**
     * 
     * @param CartItemInterface $item
     * @return int
     */
    public function addItem(CartItemInterface $item);
    
    /**
     * 
     * @param int $index
     */
    public function getItem($index);
}