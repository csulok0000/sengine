<?php

/**
 * 
 * @author Tibor Csik <csulok0000@gmail.com>
 * @copyright (c) 2015, Tibor Csik 
 */
namespace SEngine;

interface EngineInterface {
    
    /**
     * 
     * @param string $key
     * @return mixed
     */
    public function config($key);
    
    /**
     * @return AuthInterface
     */
    public function getAuth();
    
    /**
     * @return BreadCrumbsInterface
     */
    public function getBreadCrumbs();
    
    /**
     * @return ClassLoaderInterface
     */
    public function getClassLoader();
    
    /**
     * 
     * @return \Pdo
     */
    public function getDatabase();
    
    /**
     * 
     * @return ViewInterface
     */
    public function getView();
    
    /**
     * 
     * @param string $template
     * @param array $params
     * @return Response
     */
    public function render($template, $params);
    
    /**
     * 
     * @param ConfigInterface $config
     */
    public function setConfig(ConfigInterface $config);
    
    /**
     * 
     * @return ConfigInterface
     */
    public function getConfig();
    
    /**
     * 
     * @return RouterInterface
     */
    public function getRouter();
    
    /**
     * 
     * @return HelperInterface
     */
    public function dao();
    
    /**
     * 
     * @return HelperInterface
     */
    public function getDaoHelper();
    
    /**
     * 
     * @return HelperInterface
     */
    public function getModuleHelper();
    
    /**
     * 
     * @param string $message
     */
    public function addError($message);
    
    /**
     * 
     * @return array
     */
    public function getErrors();
    
    /**
     * 
     * @param RouterInterface $router
     */
    public function setRouter(RouterInterface $router);
    
    /**
     * 
     * @return SeoInterface
     */
    public function getSeo();
    
    /**
     * 
     * @return SecurityInterface
     */
    public function getSecurity();
    
    /**
     * 
     * @return RbacInterface
     */
    public function getRbac();
    
    /**
     * 
     * @param RbacInterface $rbac
     */
    public function setRbac(RbacInterface $rbac);
    
    /**
     * 
     * @return RequestInterface
     */
    public function getRequest();
    
    /**
     * 
     * @return HookInterface
     */
    public function getHook();
    
    /**
     * 
     * @param string $routeFile
     */
    public function run($routeFile = '');
    
    /**
     * 
     * @param string $type
     * @param bool $force
     * @return LoggerInterface
     */
    public function getLogger($type = 'default', $force = false);
    
    /**
     * 
     * @param ClassLoaderInterface $loader
     */
    public function setClassLoader(ClassLoaderInterface $loader);
    
    /**
     * 
     * @param \Pdo $db
     */
    public function setDatabase(\Pdo $db);
    
    /**
     * 
     * @param ViewInterface $view
     */
    public function setView(ViewInterface $view);
    
    /**
     * 
     * @param string $routeFile
     */
    public function loadRouter($routeFile);
    
}