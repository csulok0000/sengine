<?php

/**
 * 
 * @author Tibor Csik <csulok0000@gmail.com>
 */

namespace SEngine;

interface RouterInterface {
    
    /**
     * 
     * @param array $routeTable
     * @param string $request
     * @param string $host
     * @param \SEngine\EngineInterface $engine
     * @param bool $forceProcess
     */
    public function __construct($routeTable, $request = '', $host = '', EngineInterface $engine = null, $forceProcess = true);
    
    /**
     * 
     */
    public function process();
    
    /**
     * 
     * @return Route
     */
    public function getRoute();
    
    /**
     * 
     * @param string $name
     * @param array $params
     * @param bool $fullUrl
     * @return string
     */
    public function getUrl($name, $params = null, $fullUrl = false);
    
    /**
     * 
     * @param string $routeName
     * @param array $parameters
     * @return string
     */
    public function getRouteByRouteName($routeName, $parameters = array());
    
    /**
     * 
     * @return array
     */
    public function getRouteTable();
}