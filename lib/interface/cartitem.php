<?php

/**
 * 
 * @author Tibor Csik <csulok0000@gmail.com>
 * @copyright (c) 2016, Tibor Csik
 */

namespace SEngine\Lib\Cart;

interface CartItemInterface {
    
    /**
     * 
     * @param string $type
     * @return CartItemInterface
     */
    public function setItemType($type);
    
    /**
     * 
     * @param int $type
     * @return CartItemInterface
     */
    public function setItemId($type);
    
    
    /**
     * 
     * @param int $quantity
     * @return CartItemInterface
     */
    public function setQuantity($quantity);
    
    /**
     * 
     * @param int $price
     * @return CartItemInterface
     */
    public function setPrice($price);
    
    /**
     * 
     * @param int $index
     * @return CartItemInterface
     */
    public function setIndex($index);
    
    /**
     * 
     * @return string
     */
    public function getItemType();
    
    /**
     * @return int
     */
    public function getItemId();
    
    /**
     * @return int
     */
    public function getQuantity();
    
    /**
     * @return int
     */
    public function getPrice();
    
    /**
     * @return int
     */
    public function getIndex();
    
}