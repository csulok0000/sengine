<?php

/**
 * 
 * @author Tibor Csik <csulok0000@gmail.com>
 */

namespace SEngine;

interface RequestInterface {
    
    /**
     * 
     * @return bool
     */
    public function isPost();
    
    /**
     * 
     * @return bool
     */
    public function isGet();
    
    /**
     *
     * @return boolean
     */
    public function isPut();
    
    /**
     *
     * @return boolean
     */
    public function isDelete();
    
    /**
     * 
     * @param string $name
     * @return mixed
     */
    public function get($name, $trim = false);
    
    /**
     * 
     * @param string $name
     * @return mixed
     */
    public function post($name, $trim = false);

    /**
     * 
     * @param string $name
     * @return mixed
     */
    public function cookie($name, $trim = false);
    
    /**
     * 
     * @param string $name
     * @return mixed
     */
    public function server($name, $trim = false);
    
    /**
     * 
     * @param string $name
     * @return mixed
     */
    public function session($name, $trim = false);
    
    /**
     * 
     * @param string $path pl. testKey, site->url
     * @return mixed|null
     */
    public function request($path);
    
    /**
     * 
     * @param string $name
     * @return mixed
     */
    public function file($name);
    
    /**
     * 
     * @param array $file $_FILES['file']
     * @return bool
     */
    public function isValidPostFile($file);
    
    /**
     * 
     * @param array $array
     * @param string $path ex.: site, site->host
     * @return type
     */
    public function arrayValue($array, $path, $trim = false);
    
    
}
