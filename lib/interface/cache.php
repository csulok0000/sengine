<?php

/**
 * 
 * @author Tibor Csik <csulok0000@gmail.com>
 */

namespace SEngine;

interface CacheInterface {
    
    /**
     * 
     * @param string $key
     * @return mixed
     */
    public function get($key);
    
    /**
     * 
     * @param string $key
     * @param mixed $data
     * @param int $time
     */
    public function set($key, $data, $time = 600);
    
    /**
     * @param string $key
     */
    public function delete($key);
    
    /**
     * 
     * @param array $data
     * @param int $length A kulcs maximális hossza, a függvény a kulcs végéhez kapcsol egy 4 karakteres hash stringet
     * @return string
     */
    public function generateKey($data, $length = 60);
}
