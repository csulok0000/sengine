<?php

/**
 * 
 * @author Tibor Csik <csulok0000@gmail.com>
 */

namespace SEngine;

interface AuthInterface {
    
    /**
     * 
     * @param \SEngine\AuthAdapterInterface $adapter
     */
    public function setAuthAdapter(AuthAdapterInterface $adapter);
    
    /**
     * 
     * @param string $username
     * @param string $password
     * @param string $type
     * @return mixed
     */
    public function auth($username, $password, $type = 'user');
    
    /**
     * 
     */
    public function init();
    
    /**
     * 
     * @return UserInterface
     */
    public function get();
    
    /**
     * 
     * @param mixed $user
     */
    public function set($user);
    
    /**
     * 
     * @return int
     */
    public function getId();
    
    /**
     * 
     * @param int $id
     */
    public function setId($id);
}
