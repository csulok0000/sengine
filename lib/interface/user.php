<?php

/**
 * 
 * @author Tibor Csik <csulok0000@gmail.com>
 * @copyright (c) 2016, Tibor Csik
 */

namespace SEngine;

interface UserInterface {
    
    /**
     * 
     * @param int $id
     */
    public function setId($id);
    
    /**
     * 
     * @return int
     */
    public function getId();
}