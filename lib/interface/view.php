<?php

/**
 * 
 * @author Tibor Csik <csulok0000@gmail.com>
 */

namespace SEngine;

interface ViewInterface {
    
    /**
	 *
	 * @abstract
	 * @param string $templateDir
	 * @param string $cacheDir
	 * @param bool $forceReload Reload template file at everytime
	 */
	public function __construct($templateDir, $cacheDir = '', $forceReload = false);
    
    /**
	 *
	 * @abstract
	 * @param string $template
	 * @param array $params Template variables
	 * @return Response
	 */
	public function render($template, $params = array());
    
    /**
	 *
	 * @param string $name
	 * @param mixed $value
	 */
	public function addVar($name, $value);
    
    /**
	 *
	 * @param string $name
	 */
	public function removeVar($name);
}
