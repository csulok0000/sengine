<?php

/**
 * 
 * @author Tibor Csik <csulok0000@gmail.com>
 */

namespace SEngine;

interface RbacInterface {
    
    /**
     * 
     * Initialize RBAC
     * 
     * Roles:
     *  [
     *      <role name 1> => [
     *          <permission name 1>,
     *          <permission name 2>,
     *          <permission name 3>,
     *          <permission name 4>
     *      ],
     *      <role name 2> => [
     *          <permission name 1>,
     *          <permission name 2>,
     *          <permission name 3>,
     *          <permission name 4>
     *      ],
     *  ]
     * 
     * @param array $roles Roles of the entity
     */
    public function __construct(array $roles);
    
    /**
     * 
     * @param string $permissions
     * @return bool
     */
    public function hasPermission($permissions);
    
    /**
     * 
     * @param string $key
     * @param bool $isRole
     * @return bool
     */
    public function isAuthorized($key, $isRole = false);
    
    /**
     * 
     * @param string $roles
     * @return bool
     */
    public function hasRole($roles);
    
    /**
     * 
     * Clear all roles and permissions
     *  
     * @return $this
     */
    public function clear();
    
    /**
     * 
     * @return array
     */
    public function getRoles();
    
    /**
     * 
     * @param array $roles
     * @return $this
     */
    public function addRoles(array $roles);
    
    /**
     * 
     * @param string $roleName
     * @param array $permissions
     * @return $this
     */
    public function addRole($roleName, array $permissions);
    
    /**
     * 
     * @param string $roleName
     * @return $this
     */
    public function removeRole($roleName);
    
    /**
     * 
     * @return $this
     */
    public function rebuildPermissions();
}
