<?php

/**
 * 
 * @author Tibor Csik <csulok0000@gmail.com>
 * @copyright 2015 (c), Tibor Csik
 */

namespace SEngine;

interface OpenGraphInterface {
    
    /**
     * 
     * @param string $ogTitle
     */
    public function setOgTitle($ogTitle);
    
    /**
     * 
     * @param string $ogDescription
     */
    public function setOgDescription($ogDescription);
    
    /**
     * 
     * @param string $ogType
     */
    public function setOgType($ogType);
    
    /**
     * 
     * @param string $ogImage
     */
    public function setOgImage($ogImage);
    
    /**
     * 
     * @param string $ogUrl
     */
    public function setOgUrl($ogUrl);
    
    /**
     * 
     * @return string
     */
    public function getOgTitle();
    
    /**
     * 
     * @return string
     */
    public function getOgDescription();
    
    /**
     * 
     * @return string
     */
    public function getOgType();
    
    /**
     * 
     * @return string
     */
    public function getOgImage();
    
    /**
     * 
     * @return string
     */
    public function getOgUrl();
}