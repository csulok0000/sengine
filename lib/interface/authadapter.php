<?php

/**
 * 
 * @author Tibor Csik <csulok0000@gmail.com>
 * @copyright (c) 2015, Tibor Csik
 */

namespace SEngine;

interface AuthAdapterInterface {
    
    /**
     * 
     * @param EngineInterface $engine
     */
    public function __construct(EngineInterface $engine);
    
    /**
     * 
     * @param string $username
     * @param string $password
     */
    public function auth($username, $password, $type = 'user');
    
    /**
     * @return EngineInterface
     */
    public function getEngine();
    
    /**
     * Reload logged user data, if needed.
     */
    public function load();
    
    /**
     * 
     * Return the user data
     * 
     * @return UserInterface
     */
    public function get();
    
    /**
     * 
     * @param UserInterface $user
     */
    public function set($user);
}