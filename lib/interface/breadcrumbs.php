<?php

/**
 * 
 * @author Tibor Csik <csulok0000@gmail.com>
 */

namespace SEngine;

interface BreadCrumbsInterface {
    
    /**
     * 
     * @param string $name
     * @param string $link
     * @return int
     */
    public function add($name, $link = 'null');
    
    /**
     * 
     * @return array
     */
    public function toArray();
}
