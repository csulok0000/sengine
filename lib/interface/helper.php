<?php

/**
 * 
 * @author Tibor Csik <csulok0000@gmail.com>
 */

namespace SEngine;

interface HelperInterface {
    
    /**
     * 
     * @param EngineInterface $engine
     */
    public function __construct(EngineInterface $engine);
}
