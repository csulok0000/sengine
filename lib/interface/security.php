<?php

/**
 * 
 * @author Tibor Csik <csulok0000@gmail.com>
 * @copyright 2015 (c), Tibor Csik
 */

namespace SEngine;

interface SecurityInterface {
    /**
     *
     * @param string $name
     * @return array
     *
     */
    public function get($name);

    /**
     *
     * @param string $name
     * @param array $secure
     *
     */
    public function set($name, $secure);
}