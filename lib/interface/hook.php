<?php

/**
 * 
 * @author Tibor Csik <csulok0000@gmail.com>
 */

namespace SEngine;

interface HookInterface {
    
    /**
     * 
     * @param EngineInterface $engine
     * @param LoggerInterface $logger
     */
    public function __construct(EngineInterface $engine, LoggerInterface $logger = null);
    
    /**
     * 
     * @param type $hook <type>:<event> ex.: Engine:beforeRun
     * @return boolean
     */
    public function run($hook);
    
    /**
     * 
     * @param LoggerInterface $logger
     */
    public function setLogger(LoggerInterface $logger);
}
