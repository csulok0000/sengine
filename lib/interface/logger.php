<?php

/**
 * 
 * @author Tibor Csik <csulok0000@gmail.com>
 */

namespace SEngine;

interface LoggerInterface {
    
    /**
     * 
     * @param string $message
     * @return LoggerInterface
     */
    public function debug($message);
    
    /**
     * 
     * @param string $message
     * @return LoggerInterface
     */
    public function warn($message);
    
    /**
     * 
     * @param string $message
     * @return LoggerInterface
     */
    public function error($message);
    
    /**
     * 
     * @param string $message
     * @return LoggerInterface
     */
    public function fatal($message);
    
    /**
     * 
     * @param string $message
     * @return LoggerInterface
     */
    public function info($message);
}
