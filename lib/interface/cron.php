<?php

/**
 * 
 * @author Tibor Csik <csulok0000@gmail.com>
 */

namespace SEngine\Lib;

interface CronInterface {
    
    /**
     * 
     * @param \SEngine\EngineInterface $engine
     */
    public function __construct(\SEngine\EngineInterface $engine);
    
    /**
     * 
     * @param string $time
     * @param string $cronName
     */
    public function register($time, $cronName);
    
    public function work();
    
}