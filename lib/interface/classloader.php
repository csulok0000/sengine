<?php

/**
 * 
 * @author Tibor Csik <csulok0000@gmail.com>
 * @copyright (c) 2016, Tibor Csik
 */

namespace SEngine;

interface ClassLoaderInterface {
    
    /**
     * 
     * @param string $class
     * @return bool
     */
    public function load($class);
}