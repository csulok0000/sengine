<?php

/**
 * 
 * @author Tibor Csik <csulok0000@gmail.com>
 */

namespace SEngine;

interface ConfigInterface {
    
    /**
     * 
     * @param array $config
     */
    public function __construct($config, $suffix = '');
    
    /**
     * 
     * @param string $path "site->key"
     * @return mixed
     */
    public function get($path);
    
    /**
     * 
     * @param string $path
     * @param mixed $value
     */
    public function set($path, $value);
    
    /**
     * 
     * @return array
     */
    public function getConfigArray();
    
    /**
     * 
     * @param array $config
     */
    public function setConfigArray(array $config);
}
