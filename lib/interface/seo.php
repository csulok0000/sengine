<?php

/**
 * 
 * @author Tibor Csik <csulok0000@gmail.com>
 * @copyright 2015 (c), Tibor Csik
 */

namespace SEngine;

interface SeoInterface {
    
    /**
     * 
     * @param string $title
     */
    public function setTitle($title);
    
    /**
     * 
     * @param string $description
     */
    public function setMetaDescription($description);
    
    /**
     * 
     * @param string $keywords
     */
    public function setMetaKeywords($keywords);
    
    /**
     * 
     * @param type $canonicalUrl
     */
    public function setCanonicalUrl($canonicalUrl);

    /**
     * 
     * @return string
     */
    public function getTitle();
    
    /**
     * 
     * @return string
     */
    public function getMetaDescription();
    
    /**
     * 
     * @return string
     */
    public function getMetaKeywords();
    
    /**
     * 
     * @return string
     */
    public function getCanonicalUrl();
}
