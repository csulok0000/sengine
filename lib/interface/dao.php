<?php

/**
 * 
 * @author Tibor Csik <csulok0000@gmail.com>
 */

namespace SEngine\Lib;

interface DaoInterface {
    
    /**
     * 
     * @param \Pdo $db
     */
    public function __construct(\Pdo $db);
    
}
