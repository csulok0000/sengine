<?php

/**
 * 
 * @author Tibor Csik <csulok0000@gmail.com>
 */

namespace SEngine;

interface ModuleInterface {
    
    /**
     * 
     * @param EngineInterface $engine
     */
    public function __construct(EngineInterface $engine);
    
    /**
     * 
     * @param string $template
     * @param array $params
     * @return Response
     */
    public function render($template, $params = array());
    
    /**
     * 
     * @return ClassLoaderInterface
     */
    public function getClassLoader();
    
    /**
     * 
     * @return \Pdo
     */
    public function getDatabase();
    
    /**
     * 
     * Alias for getConfigParam
     * 
     * @param string $key
     * @return mixed
     */
    public function config($key);
    
    /**
     * 
     * @return HelperInterface
     */
    public function dao();
    
    /**
     * 
     * @return HelperInterface
     */
    public function getDaoHelper();
    
    /**
     * 
     * @return HelperInterface
     */
    public function getModuleHelper();
    
    /**
     * 
     * @return CacheInterface
     */
    public function getCacheObject();
    
    /**
     * 
     * @return EngineInterface
     */
    public function getEngine();
    
    /**
     * 
     * @param string $message
     */
    public function addError($message);
    
    /**
     * 
     * Return ResponseInterface
     */
    public function redirect($url);
    
    /**
     * 
     * Return ResponseInterface
     */
    public function redirectRoute($routeName, $params = null, $extra = '');
    
    /**
     * 
     * @return RouterInterface
     */
    public function getRouter();
    
    /**
     * 
     * @param string $key values: key | key->subkey
     * @return mixed
     */
    public function getConfigParam($key);
    
    /**
     * 
     * @return SecurityInterface
     */
    public function getSecurity();
    
    /**
     * 
     * @param string $type
     * @param bool $force
     * @return LoggerInterface
     */
    public function getLogger($type = 'default', $force = false);
    
    /**
     * 
     * @return array
     */
    public function getErrors();
    
    /**
     * 
     * @return RequestInterface
     */
    public function getRequest();
    
    /**
     * 
     * @param string $permissions
     * @return bool
     */
    public function hasPermission($permissions);
    
    /**
     * 
     * @param string $roles
     * @return bool
     */
    public function hasRole($roles);
    
    /**
     * 
     * @return SeoInterface
     */
    public function getSeo();
    
    /**
     * 
     * @param string $name
     * @param array $params
     * @param boolean $full_url
     * @return string
     */
    public function getUrl($name, $params = null, $full_url = false);
    
    /**
     * 
     * @return AuthInterface
     */
    public function getAuth();
    
    /**
     * 
     * @return BreadCrumbsInterface
     */
    public function getBreadCrumbs();
}
