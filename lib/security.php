<?php

/**
 * 
 * @author Tibor Csik <csulok0000@gmail.com>
 * @copyright 2015 (c), Tibor Csik
 */

namespace SEngine;

class Security implements SecurityInterface {
    /**
     *
     * Array for security
     *
     */
    private $secures = array();
    /**
     *
     * @param string $name
     * @return array
     *
     */
    public function get($name) {
        if (array_key_exists($name, $this->secures)) {
            return $this->secures[$name];
        }
        return null;
    }

    /**
     *
     * @param string $name
     * @param array $secure
     *
     */
    public function set($name, $secure) {
        $this->secures[$name] = $secure;
    }
}