<?php

/**
 * 
 * @author Tibor Csik <csulok0000@gmail.com>
 */

namespace SEngine;

class TODOException extends \Exception {
    
    public function __construct($message = null, $code = 0, \Exception $previous = null) {
        $message = 'TODO: ' . $message;
        parent::__construct($message, $code, $previous);
        }
}